# Evaluation
# Implementation of evaluation function based on Claude Shannon formulation in 1949:
# f(p) = 200(K-K')
#        + 9(Q-Q')
#        + 5(R-R')
#        + 3(B-B' + N-N')
#        + 1(P-P')
#        - 0.5(D-D' + S-S' + I-I')
#        + 0.1(M-M') + ...

# KQRBNP = number of kings, queens, rooks, bishops, knights and pawns
# D,S,I = doubled, blocked and isolated pawns
# M = Mobility (the number of legal moves)

from collections import Counter
from pieces_movement_functions import convert_simbols_to_letter
from pieces_movement_functions import convert_symbol_to_varnames

import numpy as np


def count_isolated_pawns(brd_strings, turn):
    # get columns with pawns:
    if turn == 'white':
        idx_pawns = np.where(brd_strings == ' P ')
    else:
        idx_pawns = np.where(brd_strings == ' p ')
    cols_pawns = sorted(idx_pawns[1])
    cols_isolated = []

    for col_pawn in cols_pawns:
        if col_pawn == 0:
            if (col_pawn + 1 not in cols_pawns):
                cols_isolated.append(col_pawn)
        elif col_pawn == 7:
            if (col_pawn - 1 not in cols_pawns):
                cols_isolated.append(col_pawn)
        elif (col_pawn - 1 not in cols_pawns) and (col_pawn + 1 not in cols_pawns):
            cols_isolated.append(col_pawn)
    return len(cols_isolated)


def count_doubled_pawns(brd_strings, turn):
    if turn == 'white':
        idx_pawns = np.where(brd_strings == ' P ')
    else:
        idx_pawns = np.where(brd_strings == ' p ')
    cols_pawns = sorted(idx_pawns[1])
    dd = Counter(cols_pawns)
    rep_cols = [dd[i] for i in dd if dd[i] > 1]
    return len(rep_cols)

# print('ariadna is the best')


def count_possible_movements(brd_status, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                             phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    all_pos = [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7],
               [1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7],
               [2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7],
               [3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7],
               [4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7],
               [5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7],
               [6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7],
               [7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7]]

    pieces_flatten = brd_status.flatten()

    aa_w = pieces_flatten[(
        pieces_flatten >= 1) & (pieces_flatten <= 16)]
    bb_w = pieces_flatten[(
        pieces_flatten >= 409) & (pieces_flatten <= 616)]
    pieces_attacking_white = np.concatenate((aa_w, bb_w), axis=None)

    aa_b = pieces_flatten[(
        pieces_flatten >= 17) & (pieces_flatten <= 32)]
    bb_b = pieces_flatten[(
        pieces_flatten >= 717) & (pieces_flatten <= 932)]
    pieces_attacking_black = np.concatenate((aa_b, bb_b), axis=None)

    pieces_names_white = convert_symbol_to_varnames(
        pieces_attacking_white)
    pieces_names_black = convert_symbol_to_varnames(
        pieces_attacking_black)

    available_pos_white = []
    available_piece_white = []
    available_pos_black = []
    available_piece_black = []

    counter = 0
    for piece in pieces_names_white:
        for mov in all_pos:
            if vars()[piece].new_position_posible(mov, brd_status, movements, num_movements, brd):
                available_pos_white.append(mov)
                available_piece_white.append(vars()[piece].symbol)

        counter += 1
    counter = 0
    for piece in pieces_names_black:
        for mov in all_pos:
            if vars()[piece].new_position_posible(mov, brd_status, movements, num_movements, brd):
                available_pos_black.append(mov)
                available_piece_black.append(vars()[piece].symbol)

        counter += 1
    return available_pos_white, available_pos_black, available_piece_white, available_piece_black


def position_evaluation(brd_status, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                        phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    brd_strings = convert_simbols_to_letter(brd_status)
    brd_strings_list = brd_strings.tolist()
    brd_strings_flat = [
        item for sublist in brd_strings_list for item in sublist]
    count = Counter(brd_strings_flat)
    num_isolated_pawns_white = count_isolated_pawns(brd_strings, 'white')
    num_isolated_pawns_black = count_isolated_pawns(brd_strings, 'black')
    num_doubled_pawns_white = count_doubled_pawns(brd_strings, 'white')
    num_doubled_pawns_black = count_doubled_pawns(brd_strings, 'black')
    available_pos_white, available_pos_black, available_piece_white, available_piece_black = count_possible_movements(
        brd_status, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
        phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
    num_movements_white = len(available_pos_white)
    num_movements_black = len(available_pos_black)
    available_piece_all = [*available_piece_white, *available_piece_black]
    available_pos_all = [*available_pos_white, *available_pos_black]
    evaluation_score = 200 * (count[' K '] - count[' k ']) + 9 * (count[' Q '] - count[' q ']) + 5 * (count[' R '] - count[' r ']) + \
        3 * ((count[' B '] - count[' b ']) + (count[' N '] - count[' n '])) + 1 * (count[' P '] - count[' p ']) - \
        0.5 * ((num_doubled_pawns_white - num_doubled_pawns_black) + (num_isolated_pawns_white - num_isolated_pawns_black)) + \
        0.1 * (num_movements_white - num_movements_black)
    # if turn == 'black':  # in our case white is the opposite, as we to evaluate black we look at white
    #    evaluation_score = - evaluation_score

    return evaluation_score, available_pos_white, available_pos_black, available_piece_white, available_piece_black, available_pos_all, available_piece_all
