import numpy as np
import board as br
import pieces_movement_functions as pm
import classes as cl
import gui as gui
import pandas as pd
from evaluation import position_evaluation


def import_games(game_number):

    #GAMES_FILENAME = "./pgn/Aronian.pgn"
    #GAMES_FILENAME = "./pgn/first_game_AI.pgn"
    GAMES_FILENAME = "./pgn/second_game_AI.pgn"

    #ff = "./pgn/test_promotion.pgn"
    ftext = open(GAMES_FILENAME, "r")
    all_text = ftext.read().splitlines()

    size = len(all_text)
    idx_list = [idx + 1 for idx, val in
                enumerate(all_text) if val == '']
    res = [all_text[i: j] for i, j in
           zip([0] + idx_list, idx_list +
               ([size] if idx_list[-1] != size else []))]

    all_games_notation = (list(res[i] for i in range(1, len(res), 2)))

    all_games2 = []
    for game in all_games_notation:
        all_games2.append(" ". join(game))
    all_games3 = []
    for gg in all_games2:
        li = list(gg.split(" "))
        del li[-3:]
        all_games3.append(li)

    all_movements = []
    for counter, value in enumerate(all_games3[(game_number - 1)]):
        vals_in_value = str.split(value, ".")
        val = vals_in_value[len(vals_in_value) - 1]
        all_movements.append(val)
    all_movements = list(filter(None, all_movements))
    ftext.close()
    return all_movements


# def get_piece_name(mov, color, new_position, board_status, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                    phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
#     pgn_symbol = mov[0]
#     pgn_2nd_symbol = mov[1]
#     col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

#     if (pgn_symbol == 'N') & (color == "white"):
#         if kqw.new_position_posible(new_position, board_status, movements, num_movements, brd) & kkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             if pgn_2nd_symbol.isdigit():
#                 row1 = pm.get_position(2, brd)[0]
#                 row2 = pm.get_position(7, brd)[0]
#                 row_original = int(pgn_2nd_symbol) - 1
#                 if row1 == row_original:
#                     piece_name = 'kqw'
#                 elif row2 == row_original:
#                     piece_name = 'kkw'
#             elif not pgn_2nd_symbol.isdigit():
#                 col1 = pm.get_position(2, brd)[1]
#                 col2 = pm.get_position(7, brd)[1]
#                 col_original = col_names.index(pgn_2nd_symbol)
#                 if col1 == col_original:
#                     piece_name = 'kqw'
#                 elif col2 == col_original:
#                     piece_name = 'kkw'
#         elif kqw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'kqw'
#         elif kkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'kkw'
#         else:
#             print("Movement illegal. No Knight available for this mevement")
#     if (pgn_symbol == 'N') & (color == "black"):
#         if kqb.new_position_posible(new_position, board_status, movements, num_movements, brd) & kkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             if pgn_2nd_symbol.isdigit():
#                 row1 = pm.get_position(26, brd)[0]
#                 row2 = pm.get_position(31, brd)[0]
#                 row_original = int(pgn_2nd_symbol) - 1
#                 if row1 == row_original:
#                     piece_name = 'kqb'
#                 elif row2 == row_original:
#                     piece_name = 'kkb'
#             elif not pgn_2nd_symbol.isdigit():
#                 col1 = pm.get_position(26, brd)[1]
#                 col2 = pm.get_position(31, brd)[1]
#                 col_original = col_names.index(pgn_2nd_symbol)
#                 if col1 == col_original:
#                     piece_name = 'kqb'
#                 elif col2 == col_original:
#                     piece_name = 'kkb'
#         elif kqb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'kqb'
#         elif kkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'kkb'
#         else:
#             print("Movement illegal. No Knight available for this mevement")

#     if (pgn_symbol == 'B') & (color == "white"):
#         if bqw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'bqw'
#         elif bkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'bkw'
#         else:
#             print("Movement illegal. No Bishop available for this mevement")
#     if (pgn_symbol == 'B') & (color == "black"):
#         if bqb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'bqb'
#         elif bkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'bkb'
#         else:
#             print("Movement illegal. No Bishop available for this mevement")

#     if (pgn_symbol == 'R') & (color == "white"):
#         if rqw.new_position_posible(new_position, board_status, movements, num_movements, brd) & rkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             if pgn_2nd_symbol.isdigit():
#                 row1 = pm.get_position(1, brd)[0]
#                 row2 = pm.get_position(8, brd)[0]
#                 row_original = int(pgn_2nd_symbol) - 1
#                 if row1 == row_original:
#                     piece_name = 'rqw'
#                 elif row2 == row_original:
#                     piece_name = 'rkw'
#             elif not pgn_2nd_symbol.isdigit():
#                 col1 = pm.get_position(1, brd)[1]
#                 col2 = pm.get_position(8, brd)[1]
#                 col_original = col_names.index(pgn_2nd_symbol)
#                 if col1 == col_original:
#                     piece_name = 'rqw'
#                 elif col2 == col_original:
#                     piece_name = 'rkw'
#         elif rqw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'rqw'
#         elif rkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'rkw'
#         else:
#             print("Movement illegal. No Rook available for this movement")
#     if (pgn_symbol == 'R') & (color == "black"):
#         if rqb.new_position_posible(new_position, board_status, movements, num_movements, brd) & rkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             if pgn_2nd_symbol.isdigit():
#                 row1 = pm.get_position(25, brd)[0]
#                 row2 = pm.get_position(32, brd)[0]
#                 row_original = int(pgn_2nd_symbol) - 1
#                 if row1 == row_original:
#                     piece_name = 'rqb'
#                 elif row2 == row_original:
#                     piece_name = 'rkb'
#             elif not pgn_2nd_symbol.isdigit():
#                 col1 = pm.get_position(25, brd)[1]
#                 col2 = pm.get_position(32, brd)[1]
#                 col_original = col_names.index(pgn_2nd_symbol)
#                 if col1 == col_original:
#                     piece_name = 'rqb'
#                 elif col2 == col_original:
#                     piece_name = 'rkb'
#         elif rqb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'rqb'
#         elif rkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
#             piece_name = 'rkb'
#         else:
#             print("Movement illegal. No Rook available for this mevement")

#     if (pgn_symbol == 'K') & (color == "white"):
#         piece_name = 'kw'
#     if (pgn_symbol == 'K') & (color == "black"):
#         piece_name = 'kb'

#     if (pgn_symbol == 'Q') & (color == "white"):
#         # TODO: Disambiguation
#         all_white_queens = ["qw", "qwa", "qwb",
#                             "qwc", "qwd", "qwe", "qwf", "qwg", "qwh"]
#         for queen in all_white_queens:
#             if vars()[queen].new_position_posible(new_position, board_status, movements, num_movements, brd):
#                 piece_name = queen
#     if (pgn_symbol == 'Q') & (color == "black"):
#         all_black_queens = ["qb", "qba", "qbb",
#                             "qbc", "qbd", "qbe", "qbf", "qbg", "qbh"]
#         for queen in all_black_queens:
#             if vars()[queen].new_position_posible(new_position, board_status, movements, num_movements, brd):
#                 piece_name = queen

#     return(piece_name)


# def get_all_pawns_in_col(col_letter, board_status, turn):
#     col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
#     pawns_symbols = board_status[:, col_names.index(col_letter)]
#     pawns_names = pm.convert_symbol_to_varnames(
#         pawns_symbols[pawns_symbols != 0])
#     if turn == "white":
#         all_pawns = ['paw', 'pbw', 'pcw', 'pdw', 'pew', 'pfw', 'pgw',
#                      'phw']
#     else:
#         all_pawns = ['pab', 'pbb', 'pcb', 'pdb', 'peb', 'pfb', 'pgb', 'phb']
#     # pawn_names = all_pawns in list(pawns_names)
#     pawn_names = [item for item in pawns_names if item in all_pawns]
#     return(pawn_names)


# def play_game(pgn_movements, halt_time):
#     # init variables

#     captured_pieces = []
#     num_movements = 0
#     game_move = 0
#     turn = "white"
#     brd = br.board()
#     brd.squares = np.array([[1, 2, 3, 4, 5, 6, 7, 8], [9, 10, 11, 12, 13, 14, 15, 16], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [
#         0, 0, 0, 0, 0, 0, 0, 0], [17, 18, 19, 20, 21, 22, 23, 24], [25, 26, 27, 28, 29, 30, 31, 32]], dtype=np.int32)
#     col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
#     d = dict({'K': ['king', 'k'], 'Q': ['queen', 'q'], 'B': [
#         'bishop', 'b'], 'N': ['knight', 'k'], 'R': ['rook', 'r']})
#     movements = pd.DataFrame(columns=[
#         "piece", "num_mov", "prev_row", "prev_col", "curr_row", "curr_col", "pgn_movement", "game_num_move"])
#     piece_to_promote = ""

#     rqw = cl.rook("rook_queen_white", pm.get_position(1, brd),
#                   pm.get_position(1, brd), "Active", "white", 1)
#     kqw = cl.knight("knight_queen_white", pm.get_position(2, brd),
#                     pm.get_position(2, brd), "Active", "white", 2)
#     bqw = cl.bishop("bishop_queen_white", pm.get_position(3, brd),
#                     pm.get_position(3, brd), "Active", "white", 3)
#     qw = cl.queen("queen_white", pm.get_position(4, brd),
#                   pm.get_position(4, brd), "Active", "white", 4)
#     kw = cl.king_white("king_white", pm.get_position(5, brd),
#                        pm.get_position(5, brd), "Active", "white", 5)
#     bkw = cl.bishop("bishop_king_white", pm.get_position(6, brd),
#                     pm.get_position(6, brd), "Active", "white", 6)
#     kkw = cl.knight("knight_king_white", pm.get_position(7, brd),
#                     pm.get_position(7, brd), "Active", "white", 7)
#     rkw = cl.rook("rook_king_white", pm.get_position(8, brd),
#                   pm.get_position(8, brd), "Active", "white", 8)

#     paw = cl.pawn("pawn_a_white", pm.get_position(9, brd),
#                   pm.get_position(9, brd), "Active", "white", 9)
#     pbw = cl.pawn("pawn_b_white", pm.get_position(10, brd),
#                   pm.get_position(10, brd), "Active", "white", 10)
#     pcw = cl.pawn("pawn_c_white", pm.get_position(11, brd),
#                   pm.get_position(11, brd), "Active", "white", 11)
#     pdw = cl.pawn("pawn_d_white", pm.get_position(12, brd),
#                   pm.get_position(12, brd), "Active", "white", 12)
#     pew = cl.pawn("pawn_e_white", pm.get_position(13, brd),
#                   pm.get_position(13, brd), "Active", "white", 13)
#     pfw = cl.pawn("pawn_f_white", pm.get_position(14, brd),
#                   pm.get_position(14, brd), "Active", "white", 14)
#     pgw = cl.pawn("pawn_g_white", pm.get_position(15, brd),
#                   pm.get_position(15, brd), "Active", "white", 15)
#     phw = cl.pawn("pawn_h_white", pm.get_position(16, brd),
#                   pm.get_position(16, brd), "Active", "white", 16)

#     rqb = cl.rook("rook_queen_black", pm.get_position(25, brd),
#                   pm.get_position(25, brd), "Active", "black", 25)
#     kqb = cl.knight("knight_queen_black", pm.get_position(26, brd),
#                     pm.get_position(26, brd), "Active", "black", 26)
#     bqb = cl.bishop("bishop_queen_black", pm.get_position(27, brd),
#                     pm.get_position(27, brd), "Active", "black", 27)
#     qb = cl.queen("queen_black", pm.get_position(28, brd),
#                   pm.get_position(28, brd), "Active", "black", 28)
#     kb = cl.king_black("king_black", pm.get_position(29, brd),
#                        pm.get_position(29, brd), "Active", "black", 29)
#     bkb = cl.bishop("bishop_king_black", pm.get_position(30, brd),
#                     pm.get_position(30, brd), "Active", "black", 30)
#     kkb = cl.knight("knight_king_black", pm.get_position(31, brd),
#                     pm.get_position(31, brd), "Active", "black", 31)
#     rkb = cl.rook("rook_king_black", pm.get_position(32, brd),
#                   pm.get_position(32, brd), "Active", "black", 32)

#     pab = cl.pawn("pawn_a_black", pm.get_position(17, brd),
#                   pm.get_position(17, brd), "Active", "black", 17)
#     pbb = cl.pawn("pawn_b_black", pm.get_position(18, brd),
#                   pm.get_position(18, brd), "Active", "black", 18)
#     pcb = cl.pawn("pawn_c_black", pm.get_position(19, brd),
#                   pm.get_position(19, brd), "Active", "black", 19)
#     pdb = cl.pawn("pawn_d_black", pm.get_position(20, brd),
#                   pm.get_position(20, brd), "Active", "black", 20)
#     peb = cl.pawn("pawn_e_black", pm.get_position(21, brd),
#                   pm.get_position(21, brd), "Active", "black", 21)
#     pfb = cl.pawn("pawn_f_black", pm.get_position(22, brd),
#                   pm.get_position(22, brd), "Active", "black", 22)
#     pgb = cl.pawn("pawn_g_black", pm.get_position(23, brd),
#                   pm.get_position(23, brd), "Active", "black", 23)
#     phb = cl.pawn("pawn_h_black", pm.get_position(24, brd),
#                   pm.get_position(24, brd), "Active", "black", 24)

#     qwa = cl.queen("queen_white_a", [7, 0], [7, 0], "Inactive", "white", 409)
#     qwb = cl.queen("queen_white_b", [7, 1], [7, 1], "Inactive", "white", 410)
#     qwc = cl.queen("queen_white_c", [7, 2], [7, 2], "Inactive", "white", 411)
#     qwd = cl.queen("queen_white_d", [7, 3], [7, 3], "Inactive", "white", 412)
#     qwe = cl.queen("queen_white_e", [7, 4], [7, 4], "Inactive", "white", 413)
#     qwf = cl.queen("queen_white_f", [7, 5], [7, 5], "Inactive", "white", 414)
#     qwg = cl.queen("queen_white_g", [7, 6], [7, 6], "Inactive", "white", 415)
#     qwh = cl.queen("queen_white_h", [7, 7], [7, 7], "Inactive", "white", 416)

#     qba = cl.queen("queen_black_a", [0, 0], [0, 0], "Inactive", "black", 717)
#     qbb = cl.queen("queen_black_b", [0, 1], [0, 1], "Inactive", "black", 718)
#     qbc = cl.queen("queen_black_c", [0, 2], [0, 2], "Inactive", "black", 719)
#     qbd = cl.queen("queen_black_d", [0, 3], [0, 3], "Inactive", "black", 720)
#     qbe = cl.queen("queen_black_e", [0, 4], [0, 4], "Inactive", "black", 721)
#     qbf = cl.queen("queen_black_f", [0, 5], [0, 5], "Inactive", "black", 722)
#     qbg = cl.queen("queen_black_g", [0, 6], [0, 6], "Inactive", "black", 723)
#     qbh = cl.queen("queen_black_h", [0, 7], [0, 7], "Inactive", "black", 724)

#     all_pieces = [rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                   phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb]

#     board_to_show = np.zeros((8, 8), dtype=np.int32)
#     for i in range(0, 8):
#         board_to_show[i, ] = brd.squares[7-i, ]
#     prev_brd = board_to_show

#     for mov in pgn_movements:
#         if num_movements % 2 == 0:
#             row_king_castle = 0
#             col_king_castle = 6
#             row_queen_castle = 0
#             col_queen_castle = 2
#             letter = 'w'
#             color = "white"
#         else:
#             row_king_castle = 7
#             col_king_castle = 6
#             row_queen_castle = 7
#             col_queen_castle = 2
#             letter = 'b'
#             color = "black"
#         if mov[0].islower():
#             piece_type = "pawn"
#             if mov[1] != 'x':
#                 new_row = int(mov[1]) - 1
#                 new_col = col_names.index(mov[0])
#                 new_position = [new_row, new_col]
#                 all_pawns_in_col = get_all_pawns_in_col(
#                     mov[0], brd.squares, turn)
#                 for pawn_name in all_pawns_in_col:
#                     if vars()[pawn_name].new_position_posible(new_position, brd.squares, movements, num_movements, brd):
#                         piece_name = pawn_name
#                     else:
#                         print('***** No pawn available for that movement!')
#                 end_game, num_movements, game_move,  turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                          brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 position_score, __, __, __, __, __, __ = position_evaluation(
#                     brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                     phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 prev_brd = gui.print_board_ascii(
#                     prev_brd, brd, halt_time, position_score)
#             elif mov[1] == 'x':
#                 new_row = int(mov[3]) - 1
#                 new_col = col_names.index(mov[2])
#                 new_position = [new_row, new_col]
#                 all_pawns_in_col = get_all_pawns_in_col(
#                     mov[0], brd.squares, turn)
#                 for pawn_name in all_pawns_in_col:
#                     if vars()[pawn_name].new_position_posible(new_position, brd.squares, movements, num_movements, brd):
#                         piece_name = pawn_name
#                     else:
#                         print('***** No pawn available for that movement!')
#                 end_game, num_movements, game_move,  turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                          brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 position_score, __, __, __, __, __, __ = position_evaluation(
#                     brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                     phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 prev_brd = gui.print_board_ascii(
#                     prev_brd, brd, halt_time, position_score)

#         elif not mov[0].islower():
#             if (mov == 'O-O') or (mov == 'O-O+'):
#                 piece_type = "king"
#                 new_row = row_king_castle
#                 new_col = col_king_castle
#                 piece_name = "k" + letter
#                 new_position = [new_row, new_col]
#                 end_game, num_movements, game_move,  turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                          brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 position_score, __, __, __, __, __, __ = position_evaluation(
#                     brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                     phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 prev_brd = gui.print_board_ascii(
#                     prev_brd, brd, halt_time, position_score)

#             elif (mov == 'O-O-O') or (mov == 'O-O-O+'):
#                 piece_type = "king"
#                 new_row = row_queen_castle
#                 new_col = col_queen_castle
#                 piece_name = "k" + letter
#                 new_position = [new_row, new_col]
#                 end_game, num_movements, game_move,  turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                          brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 position_score, __, __, __, __, __, __ = position_evaluation(
#                     brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                     phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                 prev_brd = gui.print_board_ascii(
#                     prev_brd, brd, halt_time, position_score)

#             else:
#                 piece_type = d[mov[0]][0]
#                 if mov[1] != 'x':
#                     if mov[2].isdigit():
#                         new_row = int(mov[2]) - 1
#                         new_col = col_names.index(mov[1])
#                         new_position = [new_row, new_col]
#                         piece_name = get_piece_name(
#                             mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                             phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                         end_game, num_movements, game_move,  turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                                  brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                         position_score, __, __, __, __, __, __ = position_evaluation(
#                             brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                             phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                         prev_brd = gui.print_board_ascii(
#                             prev_brd, brd, halt_time, position_score)
#                     elif not mov[2].isdigit():
#                         if mov[2] != 'x':
#                             new_row = int(mov[3]) - 1
#                             new_col = col_names.index(mov[2])
#                             new_position = [new_row, new_col]
#                             piece_name = get_piece_name(
#                                 mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                                 phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                             end_game, num_movements, game_move,  turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                                      brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                             position_score, __, __, __, __, __, __ = position_evaluation(
#                                 brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                                 phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                             prev_brd = gui.print_board_ascii(
#                                 prev_brd, brd, halt_time, position_score)

#                         elif mov[2] == 'x':
#                             new_row = int(mov[4]) - 1
#                             new_col = col_names.index(mov[3])
#                             new_position = [new_row, new_col]
#                             piece_name = get_piece_name(
#                                 mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                                 phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                             end_game, num_movements, game_move, turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                                     brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                             position_score, __, __, __, __, __, __ = position_evaluation(
#                                 brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                                 phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                             prev_brd = gui.print_board_ascii(
#                                 prev_brd, brd, halt_time, position_score)

#                 elif mov[1] == 'x':
#                     new_row = int(mov[3]) - 1
#                     new_col = col_names.index(mov[2])
#                     new_position = [new_row, new_col]
#                     piece_name = get_piece_name(
#                         mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                         phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                     end_game, num_movements, game_move, turn, new_piece, rkw, rqw, rkb, rqb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
#                                                                                                                                                                                             brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, piece_to_promote, rkw, rqw, rkb, rqb, mov, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                     position_score, __, __, __, __, __, __ = position_evaluation(
#                         brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#                         phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
#                     prev_brd = gui.print_board_ascii(
#                         prev_brd, brd, halt_time, position_score)

#     return movements
