# Autor: Quim Coll
# Main AI module to make the computer play chess
# Based on MiniMax search algorithm , with Alpha-Pruning implemented

from math import inf
import evaluation as ev
import numpy as np
import pieces_movement_functions as pm
import sys
import os
from config import *

############################################################################


def blockPrint():
    sys.stdout = open(os.devnull, 'w')


def enablePrint():
    sys.stdout = sys.__stdout__


def change_turn(turn):
    if turn == 'white':
        turn = 'black'
    else:
        turn = 'white'
    return turn


def calculate_all_possible_moves(turn, movements, num_movements, brd, game_move, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                                 phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    all_pos = [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7],
               [1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7],
               [2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7],
               [3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7],
               [4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7],
               [5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7],
               [6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7],
               [7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7]]

    pieces_flatten = brd.squares.flatten()
    available_pos_white = []
    available_piece_white = []
    available_pos_black = []
    available_piece_black = []

    if turn == 'white':
        aa_w = pieces_flatten[(
            pieces_flatten >= 1) & (pieces_flatten <= 16)]
        bb_w = pieces_flatten[(
            pieces_flatten >= 409) & (pieces_flatten <= 616)]
        pieces_attacking_white = np.concatenate((aa_w, bb_w), axis=None)
        pieces_names_white = pm.convert_symbol_to_varnames(
            pieces_attacking_white)
        for piece in pieces_names_white:
            for mov in all_pos:
                if vars()[piece].new_position_posible(mov, brd.squares, movements, num_movements, brd):
                    # TODO: new_pos_avoids_check should be check inside new_position_posible.
                    if vars()[piece].new_pos_avoids_check(turn, movements, num_movements, brd, mov, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece):
                        available_pos_white.append(mov)
                        available_piece_white.append(vars()[piece])
        return available_piece_white, available_pos_white

    elif turn == 'black':
        aa_b = pieces_flatten[(
            pieces_flatten >= 17) & (pieces_flatten <= 32)]
        bb_b = pieces_flatten[(
            pieces_flatten >= 717) & (pieces_flatten <= 932)]
        pieces_attacking_black = np.concatenate((aa_b, bb_b), axis=None)
        pieces_names_black = pm.convert_symbol_to_varnames(
            pieces_attacking_black)
        for piece in pieces_names_black:
            for mov in all_pos:
                if vars()[piece].new_position_posible(mov, brd.squares, movements, num_movements, brd):
                    # TODO: new_pos_avoids_check should be check inside new_position_posible.
                    if vars()[piece].new_pos_avoids_check(turn, movements, num_movements, brd, mov, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece):
                        available_pos_black.append(mov)
                        available_piece_black.append(vars()[piece])
        return available_piece_black, available_pos_black


def play_move(piece, mov, brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(piece, mov,
                                                                                                                                                                                                                                                                                                                                                                                                                                             brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, 'pgn_move_to_setup', rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
    return num_movements, movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh


def undo_move(piece, movements, num_movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    if is_castling_bool:
        piece.current_position = piece.initial_position

        if castling_type == "king_white_king_side":
            rkw.current_position = [0, 7]
            rkw.old_position = [0, 5]
            piece.old_position = [0, 6]
            brd.squares[0, 4] = 5
            brd.squares[0, 7] = 8
            brd.squares[0, 6] = 0
            brd.squares[0, 5] = 0
        elif castling_type == "king_white_queen_side":
            rqw.current_position = [0, 0]
            rqw.old_position = [0, 3]
            piece.old_position = [0, 2]
            brd.squares[0, 4] = 5
            brd.squares[0, 0] = 1
            brd.squares[0, 2] = 0
            brd.squares[0, 3] = 0
        elif castling_type == "king_black_king_side":
            rkb.current_position = [7, 7]
            rkb.old_position = [7, 5]
            piece.old_position = [7, 6]
            brd.squares[7, 4] = 29
            brd.squares[7, 7] = 32
            brd.squares[7, 6] = 0
            brd.squares[7, 5] = 0
        elif castling_type == "king_black_queen_side":
            rqb.current_position = [7, 0]
            rqb.old_position = [7, 3]
            piece.old_position = [7, 2]
            brd.squares[7, 4] = 29
            brd.squares[7, 0] = 25
            brd.squares[7, 2] = 0
            brd.squares[7, 3] = 0
    elif is_a_promotion_bool:
        promoted_piece.status = "Inactive"
        brd.squares[piece.current_position[0], piece.current_position[1]] = 0
        brd.squares[piece.old_position[0],
                    piece.old_position[1]] = piece.symbol
        piece.current_position = piece.old_position
    elif is_enpassant_bool:
        if piece.current_position[0] == 2:
            brd.squares[piece.old_position[0],
                        piece.old_position[1]] = piece.symbol
            brd.squares[piece.current_position[0],
                        piece.current_position[1]] = 0
            brd.squares[3, piece.current_position[1]] = symbol_to_capture
            old_pawn = pm.convert_symbol_to_varnames(
                [symbol_to_capture])[0]
            vars()[old_pawn].status = "Active"
            piece.current_position[0] = 3
            piece.current_position[1] = piece.old_position[1]
        elif piece.current_position[0] == 5:
            brd.squares[piece.old_position[0],
                        piece.old_position[1]] = piece.symbol
            brd.squares[piece.current_position[0],
                        piece.current_position[1]] = 0
            brd.squares[4, piece.current_position[1]] = symbol_to_capture
            old_pawn = pm.convert_symbol_to_varnames(
                [symbol_to_capture])[0]
            vars()[old_pawn].status = "Active"
            piece.current_position[0] = 4
            piece.current_position[1] = piece.old_position[1]
    else:
        brd.squares[piece.old_position[0],
                    piece.old_position[1]] = piece.symbol
        if symbol_to_capture == -1:
            brd.squares[piece.current_position[0],
                        piece.current_position[1]] = 0
        else:
            brd.squares[piece.current_position[0],
                        piece.current_position[1]] = symbol_to_capture
            old_piece = pm.convert_symbol_to_varnames(
                [symbol_to_capture])[0]
            vars()[old_piece].status = "Active"
        piece.current_position = piece.old_position
        #all_prev_pieces = movements.loc[:, "piece"]
        if piece.symbol in movements["piece"].values:
            all_prev_moves = movements[movements["piece"].values == piece.symbol]
            piece.old_position = [
                all_prev_moves.loc[all_prev_moves.index[-1], 'prev_row'], all_prev_moves.loc[all_prev_moves.index[-1], 'prev_col']]
        else:
            piece.old_position = piece.initial_position
    num_movements = num_movements - 1
    return num_movements, movements, brd.squares, piece


def minimax(depth, alpha, beta, maximizingPlayer, move, movements, num_movements, initial_board, brd, turn, game_move, piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh, cache_score=[], cache_pos=[], is_enpassant_bool=False, castling_type="", is_castling_bool=False, is_a_promotion_bool=False,  promoted_piece=""):
    if maximizingPlayer:
        if depth == 0:
            brd_sq_flat = list(brd.squares.flatten())
            if brd_sq_flat in cache_pos:
                idx = cache_pos.index(brd_sq_flat)

                return cache_score[idx], brd, movements
            else:
                score, available_pos_white, available_pos_black, available_piece_white, available_piece_black, available_pos_all, available_piece_all = ev.position_evaluation(
                    brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

                cache_score.append(score)
                cache_pos.append(brd_sq_flat)
            return score, brd, movements
        turn = change_turn(turn)
        pos_pieces, pos_movs = calculate_all_possible_moves(turn, movements, num_movements, brd, game_move, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                                                            phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
        for piece, move in zip(pos_pieces, pos_movs):
            num_movements, movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = play_move(piece, move, brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements,
                                                                                                                                                                                                                                                                                                                                                                                                         rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
            score, brd, movements = minimax(depth - 1, alpha, beta, False, move, movements, num_movements, initial_board, brd, turn, game_move, piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb,
                                            bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

            movements.drop(movements.tail(1).index, inplace=True)
            num_movements, movements, brd.squares, piece = undo_move(
                piece, movements, num_movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

            if score >= beta:
                return beta, brd, movements
            if score >= alpha:
                alpha = score
        return alpha, brd, movements

    else:
        if depth == 0:
            brd_sq_flat = list(brd.squares.flatten())
            if brd_sq_flat in cache_pos:
                idx = cache_pos.index(brd_sq_flat)

                return cache_score[idx], brd, movements
            else:
                score, available_pos_white, available_pos_black, available_piece_white, available_piece_black, available_pos_all, available_piece_all = ev.position_evaluation(
                    brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
                cache_score.append(score)
                cache_pos.append(brd_sq_flat)
            return score, brd, movements

        turn = change_turn(turn)
        pos_pieces, pos_movs = calculate_all_possible_moves(turn, movements, num_movements, brd, game_move, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece,   rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                                                            phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

        for piece, move in zip(pos_pieces, pos_movs):
            num_movements, movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = play_move(piece, move, brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements,
                                                                                                                                                                                                                                                                                                                                                                                                         rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

            # calculate score
            score, brd, movements = minimax(depth - 1, alpha, beta, True, move, movements, num_movements, initial_board, brd, turn, game_move, piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb,
                                            bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

            num_movements, movements, brd.squares, piece = undo_move(
                piece, movements, num_movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
            movements.drop(movements.tail(1).index, inplace=True)
            if score <= alpha:
                return alpha, brd, movements
            if score < beta:
                beta = score
        return beta, brd, movements


def minimaxRoot(depth, alpha, beta, maximizingPlayer, move, movements, num_movements, initial_board, brd, turn, game_move, piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh, cache_score=[], cache_pos=[], is_enpassant_bool=False, castling_type="", is_castling_bool=False, is_a_promotion_bool=False,  promoted_piece=""):
    pos_pieces, pos_movs = calculate_all_possible_moves(turn, movements, num_movements, brd, game_move, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                                                        phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
    beta = 9999
    for piece, move in zip(pos_pieces, pos_movs):
        # play move
        num_movements, movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = play_move(piece, move, brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements,
                                                                                                                                                                                                                                                                                                                                                                                                     rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

        # calculate score
        score, brd, movements = minimax(depth, alpha, beta, maximizingPlayer, move, movements, num_movements, initial_board, brd, turn, game_move, piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb,
                                        bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)

        num_movements, movements, brd.squares, piece = undo_move(
            piece, movements, num_movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
        movements.drop(movements.tail(1).index, inplace=True)

        if score < beta:
            best_piece = piece
            best_move = move
            beta = score

        if score == -inf: #Checkmate
            return best_piece, best_move, beta
    return best_piece, best_move, beta