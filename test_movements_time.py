# test evaluation based on all possible movement:

# for  rook:
import numpy as np
import board as br
import pieces_movement_functions as pm
import classes as cl
import gui as gui
import pandas as pd
import time

captured_pieces = []
num_movements = 0
game_move = 0
turn = "white"
color = "black"
brd = br.board()
brd.squares = np.array([[1, 2, 3, 4, 5, 6, 7, 8], [0, 10, 11, 12, 13, 14, 15, 16], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0], [17, 18, 19, 20, 21, 22, 23, 24], [25, 26, 27, 28, 29, 30, 31, 32]], dtype=np.int32)
print(brd.squares)

col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
d = dict({'K': ['king', 'k'], 'Q': ['queen', 'q'], 'B': [
    'bishop', 'b'], 'N': ['knight', 'k'], 'R': ['rook', 'r']})
movements = pd.DataFrame(columns=[
    "piece", "num_mov", "prev_row", "prev_col", "curr_row", "curr_col", "pgn_movement", "game_num_move"])
movements.loc[0, :] = [2, 1, 1, 1, 2, 2, 3, 2]
piece_to_promote = ""

rqw = cl.rook("rook_queen_white", [0, 0], "Active", "white", 1)
kqw = cl.knight("knight_queen_white", [0, 1], "Active", "white", 2)
bqw = cl.bishop("bishop_queen_white", [0, 2], "Active", "white", 3)
qw = cl.queen("queen_white", [0, 3], "Active", "white", 4)
kw = cl.king_white("king_white", [0, 4], "Active", "white", 5)
bkw = cl.bishop("bishop_king_white", [0, 5], "Active", "white", 6)
kkw = cl.knight("knight_king_white", [0, 6], "Active", "white", 7)
rkw = cl.rook("rook_king_white", [0, 7], "Active", "white", 8)

paw = cl.pawn("pawn_a_white", [1, 0], "Active", "white", 9)
pbw = cl.pawn("pawn_b_white", [1, 1], "Active", "white", 10)
pcw = cl.pawn("pawn_c_white", [1, 2], "Active", "white", 11)
pdw = cl.pawn("pawn_d_white", [1, 3], "Active", "white", 12)
pew = cl.pawn("pawn_e_white", [1, 4], "Active", "white", 13)
pfw = cl.pawn("pawn_f_white", [1, 5], "Active", "white", 14)
pgw = cl.pawn("pawn_g_white", [1, 6], "Active", "white", 15)
phw = cl.pawn("pawn_h_white", [1, 7], "Active", "white", 16)

rqb = cl.rook("rook_queen_black", [7, 0], "Active", "black", 25)
kqb = cl.knight("knight_queen_black", [7, 1], "Active", "black", 26)
bqb = cl.bishop("bishop_queen_black", [7, 2], "Active", "black", 27)
qb = cl.queen("queen_black", [7, 3], "Active", "black", 28)
kb = cl.king_black("king_black", [7, 4], "Active", "black", 29)
bkb = cl.bishop("bishop_king_black", [7, 5], "Active", "black", 30)
kkb = cl.knight("knight_king_black", [7, 6], "Active", "black", 31)
rkb = cl.rook("rook_king_black", [7, 7], "Active", "black", 32)

pab = cl.pawn("pawn_a_black", [6, 0], "Active", "black", 17)
pbb = cl.pawn("pawn_b_black", [6, 1], "Active", "black", 18)
pcb = cl.pawn("pawn_c_black", [6, 2], "Active", "black", 19)
pdb = cl.pawn("pawn_d_black", [6, 3], "Active", "black", 20)
peb = cl.pawn("pawn_e_black", [6, 4], "Active", "black", 21)
pfb = cl.pawn("pawn_f_black", [6, 5], "Active", "black", 22)
pgb = cl.pawn("pawn_g_black", [6, 6], "Active", "black", 23)
phb = cl.pawn("pawn_h_black", [6, 7], "Active", "black", 24)

qwa = cl.queen("queen_white_a", [7, 0], "Inactive", "white", 409)
qwb = cl.queen("queen_white_b", [7, 1], "Inactive", "white", 410)
qwc = cl.queen("queen_white_c", [7, 2], "Inactive", "white", 411)
qwd = cl.queen("queen_white_d", [7, 3], "Inactive", "white", 412)
qwe = cl.queen("queen_white_e", [7, 4], "Inactive", "white", 413)
qwf = cl.queen("queen_white_f", [7, 5], "Inactive", "white", 414)
qwg = cl.queen("queen_white_g", [7, 6], "Inactive", "white", 415)
qwh = cl.queen("queen_white_h", [7, 7], "Inactive", "white", 416)

qba = cl.queen("queen_black_a", [0, 0], "Inactive", "black", 717)
qbb = cl.queen("queen_black_b", [0, 1], "Inactive", "black", 718)
qbc = cl.queen("queen_black_c", [0, 2], "Inactive", "black", 719)
qbd = cl.queen("queen_black_d", [0, 3], "Inactive", "black", 720)
qbe = cl.queen("queen_black_e", [0, 4], "Inactive", "black", 721)
qbf = cl.queen("queen_black_f", [0, 5], "Inactive", "black", 722)
qbg = cl.queen("queen_black_g", [0, 6], "Inactive", "black", 723)
qbh = cl.queen("queen_black_h", [0, 7], "Inactive", "black", 724)

# piece_directions = np.array(
#     ([1, 0], [0, 1], [-1, 0], [0, -1], [1, 1], [1, 1], [-1, 1], [1, -1]))
# piece_distances = np.array((list(range(1, 9))))
# all_pos = []
# for dist in piece_distances:
#     for dirs in piece_directions:
#         if (dirs * dist) > 0:
#             res = dirs * dist - 1
#         else:
#             res = dirs * dist + 1
#         all_pos.append(list(dirs * dist))
all_pos = [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7],
           [1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7],
           [2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7],
           [3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7],
           [4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7],
           [5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7],
           [6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7],
           [7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7]]

print(all_pos)
available_pos = []

# all_pieces = [rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
#              phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb]

pieces_flatten = brd.squares.flatten()
if color == "white":
    aa = pieces_flatten[(
        pieces_flatten >= 17) & (pieces_flatten <= 32)]
    bb = pieces_flatten[(
        pieces_flatten >= 717) & (pieces_flatten <= 932)]
    pieces_attacking = np.concatenate((aa, bb), axis=None)

elif color == "black":
    aa = pieces_flatten[(
        pieces_flatten >= 1) & (pieces_flatten <= 16)]
    bb = pieces_flatten[(
        pieces_flatten >= 409) & (pieces_flatten <= 616)]
    pieces_attacking = np.concatenate((aa, bb), axis=None)
pieces_names = pm.convert_symbol_to_varnames(
    pieces_attacking)
start_time = time.time()
counter = 0

for piece in pieces_names:
    for mov in all_pos:
        if vars()[piece].new_position_posible(mov, brd.squares, movements, num_movements, brd):
            available_pos.append(mov)

    counter += 1
    print(
        '***********************************************************' + piece + str(counter))

print(available_pos)
print("--- %s seconds ---" % (time.time() - start_time))
