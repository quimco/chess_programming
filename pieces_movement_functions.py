# Autor: Quim Coll
# Defines all functions needed to move pieces

import pandas as pd

# Handle pawn promotion , creating a new piece (queen, knight, or bishop) when a pawn reaches the opposite end of the board. 
# It determines the color of the new piece based on the pawn's position, creates the new piece with a unique identifier, and returns the new piece.
def create_piece_promoted(new_piece_name, pawn_code, new_position):
    import classes as cl
    row = new_position[0]
    col = new_position[1]
    if row == 7:
        color = "white"
        if new_piece_name == "queen":
            new_piece = cl.queen("queen_new", [row, col], [row, col], "Active", color, int(
                str(40) + str(pawn_code)))
        elif new_piece_name == "knight":
            new_piece = cl.knight("knight_new", [row, col], [row, col], "Active", color, int(
                str(50) + str(pawn_code)))
        elif new_piece_name == "bishop":
            new_piece = cl.bishop("bishop_new", [row, col], [row, col], "Active", color, int(
                str(60) + str(pawn_code)))
    elif row == 0:
        color = "black"
        if new_piece_name == "queen":
            new_piece = cl.queen("queen_new", [row, col], [row, col], "Active", color, int(
                str(70) + str(pawn_code)))
        elif new_piece_name == "knight":
            new_piece = cl.knight("knight_new", [row, col], [row, col], "Active", color, int(
                str(80) + str(pawn_code)))
        elif new_piece_name == "bishop":
            new_piece = cl.bishop("bishop_new", [row, col], [row, col], "Active", color, int(
                str(90) + str(pawn_code)))

    return(new_piece)

# This function is used to convert numeric position to algebraic notation
def get_position(number, brd):
    import numpy as np
    position = np.where(brd.squares == number)
    pos0 = position[0].item()
    pos1 = position[1].item()
    return([pos0, pos1])

# This function is used 
def convert_symbol_to_varnames(wanted_symbols):
    import numpy as np
    all_pieces_names = np.array(["rqw", "kqw", "bqw", "qw", "kw", "bkw", "kkw", "rkw", "paw", "pbw", "pcw", "pdw", "pew", "pfw", "pgw",
                                 "phw", "pab", "pbb", "pcb", "pdb", "peb", "pfb", "pgb", "phb", "rqb", "kqb", "bqb", "qb", "kb", "bkb", "kkb", "rkb",
                                 "qwa", "qwb", "qwc", "qwd", "qwe", "qwf", "qwg", "qwh", "qba", "qbb", "qbc", "qbd", "qbe", "qbf", "qbg", "qbh"])
    all_symbols = np.array([*range(1, 33), *range(409, 417), *range(717, 725)])
    dict_symb_names = dict(zip(all_symbols, all_pieces_names))
    values_wanted = [dict_symb_names[x] for x in wanted_symbols]
    return values_wanted


def convert_simbols_to_letter(board_to_show):
    import numpy as np
    # string_symbols = ["\u2654", " N ", " B ", " Q ", " K ", " B ", " N ", " R ", " P ", " P ", " P ", " P ", " P ", " P ",
    #                  " P ", " P ", " p ", " p ", " p ", " p ", " p ", " p ", " p ", " p ", " r ", " n ", " b ", " q ", " k ", " b ", " n ", " r "]
    string_symbols = [" R ", " N ", " B ", " Q ", " K ", " B ", " N ", " R ", " P ", " P ", " P ", " P ", " P ", " P ",
                      " P ", " P ", " p ", " p ", " p ", " p ", " p ", " p ", " p ", " p ", " r ", " n ", " b ", " q ", " k ", " b ", " n ", " r "]

    str_board = board_to_show.astype(str)
    for i in range(1, 65):
        itemindex = np.where(board_to_show == 0)
        str_board[itemindex] = "   "
    for i in range(1, 33):
        itemindex = np.where(board_to_show == i)
        str_board[itemindex] = string_symbols[i-1]
        # print(i)
    # Cases when promoting. We cover all possible cases
    # TODO: this is highly inefficient. Recode!
    for i in range(409, 417):
        itemindex = np.where(board_to_show == i)
        str_board[itemindex] = " Q "
    for i in range(509, 517):
        itemindex = np.where(board_to_show == i)
        str_board[itemindex] = " N "
    for i in range(609, 617):
        itemindex = np.where(board_to_show == i)
        str_board[itemindex] = " B "
    for i in range(717, 725):
        itemindex = np.where(board_to_show == i)
        str_board[itemindex] = " q "
    for i in range(817, 825):
        itemindex = np.where(board_to_show == i)
        str_board[itemindex] = " n "
    for i in range(917, 925):
        itemindex = np.where(board_to_show == i)
        str_board[itemindex] = " b "
    return(str_board)

# This function is used to define if a square is attacked by a piece of a given color
def square_attacked(piece_to_check, color, square_to_check, movements, num_movements, brd):

    # TODO: How can initilize pieces and aviod this:
    import classes as cl
    import numpy as np
    from config import rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh

    pieces_flatten = brd.squares.flatten()
    if color == "white":
        aa = pieces_flatten[(
            pieces_flatten >= 17) & (pieces_flatten <= 32)]
        bb = pieces_flatten[(
            pieces_flatten >= 717) & (pieces_flatten <= 932)]
        pieces_attacking = np.concatenate((aa, bb), axis=None)

    elif color == "black":
        aa = pieces_flatten[(
            pieces_flatten >= 1) & (pieces_flatten <= 16)]
        bb = pieces_flatten[(
            pieces_flatten >= 409) & (pieces_flatten <= 616)]
        pieces_attacking = np.concatenate((aa, bb), axis=None)
    pieces_attacking_names = convert_symbol_to_varnames(
        pieces_attacking)

    idx_w = np.where(brd.squares == 5)
    idx_b = np.where(brd.squares == 29)

    for piece in pieces_attacking_names:
        vars()[piece].current_position = get_position(
            vars()[piece].symbol, brd)
        if color == 'white':
            if piece_to_check != 5:
                brd.squares[idx_w] = 0
                if vars()[piece].new_position_posible(
                        square_to_check, brd.squares, movements, num_movements, brd):
                    brd.squares[idx_w] = 5
                    return True
                brd.squares[idx_w] = 5
            else:
                if vars()[piece].new_position_posible(
                        square_to_check, brd.squares, movements, num_movements, brd):
                    return True
        else:
            if piece_to_check != 29:
                brd.squares[idx_b] = 0
                if vars()[piece].new_position_posible(
                        square_to_check, brd.squares, movements, num_movements, brd):
                    brd.squares[idx_b] = 29
                    return True
                brd.squares[idx_b] = 29
            else:
                if vars()[piece].new_position_posible(
                        square_to_check, brd.squares, movements, num_movements, brd):
                    return True

    return False


def square_attacked_2(color, square_to_check, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):

    # TODO: How can initilize pieces and aviod this:
    import classes as cl
    import numpy as np

    pieces_flatten = brd.squares.flatten()
    if color == "white":
        aa = pieces_flatten[(
            pieces_flatten >= 17) & (pieces_flatten <= 32)]
        bb = pieces_flatten[(
            pieces_flatten >= 717) & (pieces_flatten <= 932)]
        pieces_attacking = np.concatenate((aa, bb), axis=None)

    elif color == "black":
        aa = pieces_flatten[(
            pieces_flatten >= 1) & (pieces_flatten <= 16)]
        bb = pieces_flatten[(
            pieces_flatten >= 409) & (pieces_flatten <= 616)]
        pieces_attacking = np.concatenate((aa, bb), axis=None)
    pieces_attacking_names = convert_symbol_to_varnames(
        pieces_attacking)

    idx_w = np.where(brd.squares == 5)
    idx_b = np.where(brd.squares == 29)

    for piece in pieces_attacking_names:
        if color == 'white':
            brd.squares[idx_w] = 0
            if vars()[piece].symbol in range(17, 25):
                if vars()[piece].is_attacking(square_to_check):
                    brd.squares[idx_w] = 5
                    return True
                brd.squares[idx_w] = 5
            else:
                if vars()[piece].new_position_posible(
                        square_to_check, brd.squares, movements, num_movements, brd):
                    brd.squares[idx_w] = 5
                    return True
                brd.squares[idx_w] = 5
        else:
            brd.squares[idx_b] = 0
            if vars()[piece].symbol in range(9, 17):
                if vars()[piece].is_attacking(square_to_check):
                    brd.squares[idx_b] = 29
                    return True
                brd.squares[idx_b] = 29
            else:
                if vars()[piece].new_position_posible(
                        square_to_check, brd.squares, movements, num_movements, brd):
                    brd.squares[idx_b] = 29
                    return True
                brd.squares[idx_b] = 29
    return False


def new_pos_avoids_check_2(piece, turn, movements, num_movements, brd, wanted_pos, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    import copy
    aux_board = copy.deepcopy(brd)
    aux_board.update_board(piece.current_position, wanted_pos, turn,
                           is_enpassant_bool, piece.symbol, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece)
    if turn == "white":
        king_symbol = 5
    else:
        king_symbol = 29
    pos_king = get_position(king_symbol, aux_board)
    king_in_check_bool = square_attacked_2(
        turn, pos_king, movements, num_movements, aux_board, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
    return(not king_in_check_bool)


def capture_piece(captured_pieces, captured_piece, all_pieces):
    captured_pieces.append(captured_piece)
    # TODO: this ios higly ineffieient! improve.
    for piece in all_pieces:
        if (piece.symbol == captured_piece):
            piece.status = "Inactive"


def update_movements(old_pos, new_pos, symbol, movements, num_movements, pgn_mov, game_num_move):

    movements.loc[num_movements, ["piece", "num_mov", "prev_row", "prev_col", "curr_row", "curr_col", "pgn_movement", "game_num_move"]] = (symbol, num_movements,
                                                                                                                                           old_pos[0], old_pos[1], new_pos[0], new_pos[1], pgn_mov, game_num_move)
    # row_list = []
    # if num_movements != 1:
    #     for i in range(num_movements-1):
    #         row_list.append({"piece": movements.loc[i, "piece"], "num_mov": movements.loc[i, "num_mov"], "prev_row": movements.loc[i, "prev_row"], "prev_col": movements.loc[i, "prev_col"],
    #                          "curr_row": movements.loc[i, "curr_row"], "curr_col": movements.loc[i, "curr_col"], "pgn_movement": movements.loc[i, "pgn_movement"], "game_num_move": movements.loc[i, "game_num_move"]})

    # dict1 = {"piece": symbol, "num_mov": num_movements, "prev_row": old_pos[0], "prev_col": old_pos[1],
    #          "curr_row": new_pos[0], "curr_col": new_pos[1], "pgn_movement": pgn_mov, "game_num_move": game_num_move}
    # row_list.append(dict1)
    # movements = pd.DataFrame(row_list, columns=[
    #     "piece", "num_mov", "prev_row", "prev_col", "curr_row", "curr_col", "pgn_movement", "game_num_move"])
    return movements
    # movements.append([symbol, num_movements, old_pos[0], old_pos[1],
    #                  new_pos[0], new_pos[1], pgn_mov, game_num_move])


def move_piece(piece_to_move, wanted_pos, brd, all_pieces, num_movements,  game_move, turn, captured_pieces, movements, pgn_mov, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    is_enpassant_bool = False
    castling_type = ""
    is_castling_bool = False
    end_game = False
    new_piece = ""
    is_a_promotion_bool = False
    symbol_to_capture = -1
    promoted_piece = ""
    if piece_to_move == "":  # check if it's tryin to move promoted piece when there has not been promotion actually
        print("Illegal move, there has not been any promotion yet. Try again.")
    else:
        if piece_to_move.color != turn:
            print("It's turn for ", turn, ".Please select a ", turn, "piece.")
        else:
            if piece_to_move.status == "Inactive":
                print(piece_to_move.name,
                      " is not in the board (has been previously captured). Please, choose another piece to move.")
            else:
                if piece_to_move.new_position_posible(wanted_pos, brd.squares, movements, num_movements, brd):
                    is_a_promotion_bool = (piece_to_move.symbol in range(9, 25)) & (
                        (wanted_pos[0] == 0) or (wanted_pos[0] == 7))
                    # "create" new piece if it's a promotion:
                    if is_a_promotion_bool:
                        col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
                        letter = 'w' if turn == 'white' else 'b'
                        new_piece_name = 'q' + letter + \
                            col_names[wanted_pos[1]]
                        promoted_piece = vars()[new_piece_name]
                        promoted_piece.status = "Active"

                    # check is enpassant:
                    is_pawn_bool = piece_to_move.symbol in range(9, 25)
                    if is_pawn_bool:
                        is_enpassant_bool = piece_to_move.is_enpassant(
                            wanted_pos, brd.squares, movements, num_movements)
                    # check if it's castling move if it's a king:
                    is_king_bool = piece_to_move.symbol in [5, 29]
                    if is_king_bool:
                        castling_type, is_castling_bool = piece_to_move.is_castling(
                            wanted_pos, brd.squares, movements, num_movements, brd)

                    # if piece_to_move.new_pos_avoids_check(turn, movements, num_movements, brd, wanted_pos, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece):
                    if new_pos_avoids_check_2(piece_to_move, turn, movements, num_movements, brd, wanted_pos, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):

                        if piece_to_move.color == turn:  # move only if its' your turn
                            # Update turn and number of movement:

                            # check if it's enpassant move if it's a pawn:

                            if is_enpassant_bool:
                                if turn == "white":
                                    symbol_to_capture = brd.squares[wanted_pos[0] -
                                                                    1, wanted_pos[1]]
                                elif turn == "black":
                                    symbol_to_capture = brd.squares[wanted_pos[0] +
                                                                    1, wanted_pos[1]]
                                capture_piece(captured_pieces,
                                              symbol_to_capture, all_pieces)

                            # perform actions once movement is accepted:

                            # perform movement:
                            old_pos, new_pos = piece_to_move.new_position(
                                wanted_pos, brd.squares, is_castling_bool, castling_type)
                            print(piece_to_move.name, "moves to: ", new_pos)

                            if is_castling_bool:
                                __, __ = rkw.new_position(
                                    wanted_pos, brd.squares, is_castling_bool, castling_type)
                                __, __ = rqw.new_position(
                                    wanted_pos, brd.squares, is_castling_bool, castling_type)
                                __, __ = rkb.new_position(
                                    wanted_pos, brd.squares, is_castling_bool, castling_type)
                                __, __ = rqb.new_position(
                                    wanted_pos, brd.squares, is_castling_bool, castling_type)

                            # perform capture if needed:
                            if not piece_to_move.position_empty(wanted_pos, brd.squares):
                                symbol_to_capture = int(brd.squares[wanted_pos[0],
                                                                    wanted_pos[1]])
                                capture_piece(captured_pieces,
                                              symbol_to_capture, all_pieces)
                                print("--It's a capture")

                            # Update board:
                            ## this line updates the board!! So, to avoid confusion, we have to return the var brd in this function. Fixed.
                            brd.update_board(old_pos, new_pos,
                                             turn, is_enpassant_bool, piece_to_move.symbol, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece)

                            # update counters:
                            num_movements += 1
                            if num_movements % 2 == 0:
                                turn = "white"
                            else:
                                turn = "black"
                                game_move += 1
                            movements = update_movements(
                                old_pos, new_pos, piece_to_move.symbol, movements, num_movements, pgn_mov, game_move)

                else:
                    print("Movement Illegal. please choose another movement")

    return is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements, end_game, num_movements, game_move, turn, new_piece, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh
