# Chess_Programming

First approach to a Chess Engine.

## Main goal:

- Implement a Chess Move Generator and Chess Engine from scratch

## Conditions:

- I can look at any Python and programming resource
- I cannot look at any previous chess implementation (this is an intellectual challenge, what's the sense of looking at the solution! :)

## Done:

- Chess Move Genarator
    - [x] Piece movement
    - [x] Castling
    - [x] en-passant
    - [x] Promotion
    - [x] Check
    - [x] ASCII GUI

- Evalution Module
    - [x] Position Evaluation based on material and movility (Claude Shannon formulation)

- Search Module ("AI")
    - [x] MiniMax search algorithm implemented
    - [x] Alpha-Pruning implemented
    - [x] Transpositional tables implemented

- Other:
    - [x] PGN importer

## Ongoing / Next:

- [ ] Improve GUI
- [ ] Improve Evaluation efficiency
- [ ] Improve play interaction


## About me:

- I'm a Data Scientist with high exposure to R programming, Machine Learning and Data Analysis
- I am completed biased towards non OOP programming, as I studied C / algorithmics in the University (sooo time ago! :)
- I'm a Chess fan

## Note:
This project is being developed during the COVID-19 pandemia. Basically, is a good way to stay intellectually active, learn new skills, and stay psychologycally strong during the confinement (60 days so far and counting... take care and STAY HOME!)



## Examples

You can play against the Engine executing $python3 play_framework.py
Beware that you have to enter your movements in PGN format. By now, you play white, and depth is by default 2 so every machine moves takes 2-10 seconds.

You can also Import PGN games from Aronian Historical DDBB executing $python3 auto_pay.py