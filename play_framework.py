# Autor: Quim Coll
# Main intreface to play chess with the computer
# Run "play_framework.py" and enjoy!

import numpy as np
from config import * # Initialize all variables, including pieces and board
from play_functions import play
from minimax_framework import minimaxRoot
from minimax_framework import play_move
from minimax_framework import blockPrint
from minimax_framework import enablePrint
from minimax_framework import change_turn
from gui import print_board_ascii
import math
import cProfile
import re

turn = "black"


board_to_show = np.zeros((8, 8), dtype=np.int32)
for i in range(0, 8):
    board_to_show[i, ] = brd.squares[7-i, ]
prev_brd = board_to_show
halt_time = 0
score = 0
all_pieces_args = [rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh]

while not(end_game):
    turn = change_turn(turn)
    enablePrint()
    man_move = input(" Your turn; Enter movement in PGN format: ")
    blockPrint()
    #man_move = 'e4'
    while True:
        try:
            man_move, captured_pieces, num_movements, game_move, turn, brd, col_names, d, movements, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = play(man_move, halt_time, captured_pieces, num_movements, game_move, turn, brd, col_names, d, movements, *all_pieces_args)
            break
        except Exception as e:
            enablePrint()
            print("Illegal move, please try again")
            man_move = input(" Your turn; Enter movement in PGN format: ")
            blockPrint()
    enablePrint()
    prev_brd = print_board_ascii(
        prev_brd, brd, halt_time, score)
    blockPrint()
    # machine move:

    best_piece, best_move, score = minimaxRoot(1, -math.inf, math.inf, True, move, movements, num_movements, initial_board, brd, turn, game_move, piece, *all_pieces_args)
    #cProfile.run('minimaxRoot(1, -math.inf, math.inf, True, move, movements, num_movements, initial_board, brd, turn, game_move, piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb,   bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)', 'restats_depth1_minimaxRoot_square_at')

    enablePrint()

    #turn = change_turn(turn)
    num_movements, movements, is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = play_move(best_piece, best_move, brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements,*all_pieces_args)
    enablePrint()
    prev_brd = print_board_ascii(
        prev_brd, brd, halt_time, score)
    blockPrint()
