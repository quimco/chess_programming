# UI Intreface for the chess program
# Prints ASCII characters in the terminal

import board as br
import numpy as np
from os import system
from time import sleep
from pieces_movement_functions import convert_simbols_to_letter
import sys
import os


def blockPrint():
    sys.stdout = open(os.devnull, 'w')


def enablePrint():
    sys.stdout = sys.__stdout__


brd = br.board()


def print_ascii_by_one(idx_higlight, curr_state):
    kk = 0
    print('+---+---+---+---+---+---+---+---+')
    for ii in range(8):
        for jj in range(8):
            if ((ii != idx_higlight[0][0]) or (jj != idx_higlight[1][0])) & ((ii != idx_higlight[0][1]) or (jj != idx_higlight[1][1])):
                print('|' + curr_state[ii, jj], end='')
                kk += 1
                if kk % 8 == 0:
                    print('|\n' + '+---+---+---+---+---+---+---+---+')
            elif ((ii == idx_higlight[0][0]) & (jj == idx_higlight[1][0])) or ((ii == idx_higlight[0][1]) & (jj == idx_higlight[1][1])):
                print('|' + '\x1b[6;37;44m' +
                      curr_state[ii, jj] + '\x1b[0m', end='')
                kk += 1
                if kk % 8 == 0:
                    print('|\n' + '+---+---+---+---+---+---+---+---+')


def print_board_ascii(prev_brd, brd, halt_time, position_score):
    board_to_show = np.zeros((8, 8), dtype=np.int32)
    for i in range(0, 8):
        board_to_show[i, ] = brd.squares[7-i, ]
    brd_change = prev_brd - board_to_show
    idx_higlight = np.where(brd_change != 0)
    curr_state = convert_simbols_to_letter(board_to_show)
    # system('clear')
    # enablePrint()
    print_ascii_by_one(idx_higlight, curr_state)
    #print('Position Evaluation: ' + str(round(position_score, 2)))
    sleep(halt_time)
    # blockPrint()
    return board_to_show
