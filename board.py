# Autor: Quim Coll
# Define board as class, with method update_board 

import numpy as np
import pieces_movement_functions as pm


class board():
    squares = np.zeros((8, 8), dtype=np.int32)

    def update_board(self, old_position, new_position, turn, is_enpassant_bool, piece_code, castling_type, is_castling_move, is_a_promotion, promoted_piece):
        self.squares[old_position[0], old_position[1]] = 0
        self.squares[new_position[0], new_position[1]] = piece_code
        if is_enpassant_bool:
            if turn == "white":
                self.squares[new_position[0] - 1, new_position[1]] = 0
            elif turn == "black":
                self.squares[new_position[0] + 1, new_position[1]] = 0
        elif is_castling_move:
            if castling_type == "king_white_king_side":
                self.squares[0, 4] = 0
                self.squares[0, 7] = 0
                self.squares[0, 6] = 5
                self.squares[0, 5] = 8
            elif castling_type == "king_white_queen_side":
                self.squares[0, 4] = 0
                self.squares[0, 0] = 0
                self.squares[0, 2] = 5
                self.squares[0, 3] = 1
            elif castling_type == "king_black_king_side":
                self.squares[7, 4] = 0
                self.squares[7, 7] = 0
                self.squares[7, 6] = 29
                self.squares[7, 5] = 32
            elif castling_type == "king_black_queen_side":
                self.squares[7, 4] = 0
                self.squares[7, 0] = 0
                self.squares[7, 2] = 29
                self.squares[7, 3] = 25
        elif is_a_promotion:
            self.squares[old_position[0], old_position[1]] = 0
            self.squares[new_position[0],
                         new_position[1]] = promoted_piece.symbol
