from PGN_importer import import_games
from play_functions import play
import numpy as np
from config import *

# Number of game to import
game_movs = import_games(1)

# Play the game.
halt_time = 0
score = 0

for mov in game_movs:
    mov, captured_pieces, num_movements, game_move, turn, brd, col_names, d, movements, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = play(mov, halt_time, captured_pieces, num_movements, game_move, turn, brd, col_names, d, movements, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                                                                                                                                                                                                                                                                                                                                          phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
