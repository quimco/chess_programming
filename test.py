# test:

import PGN_importer as pg
import time

t = time.time()
# Number of game to import
num_games = list(range(1, 10))
messages = []
counter = 0
# Play the game.
for game_num in num_games:
    input_game_movs = pg.import_games(game_num)
    try:
        movements = pg.play_game(input_game_movs, 0)
        mov = movements.loc[:, 'pgn_movement']
        output_game_movs = list(mm for mm in mov)
        output_message = "Game " + str(game_num) + " has finished all movements right?: " + \
            str(input_game_movs == output_game_movs)
        messages.append(output_message)
        counter += 1
    except:
        output_message = "*** Game " + \
            str(game_num) + " has finished with error"
        messages.append(output_message)
for mess in messages:
    print(mess)
print(counter)
print((time.time()-t))
