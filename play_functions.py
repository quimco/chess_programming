# Autor: QUim Coll
# Defines all functions needed to make movements, including:
#   get_piece_name: retuns name of piece (ex "kqw" for  Knight Queen White")
#   get_all_pawns_in_col: return the names of pawns in a column
#   play: main function to make a move

import numpy as np
import pieces_movement_functions as pm
import gui as gui
import pandas as pd

def get_piece_name(mov, color, new_position, board_status, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                   phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    
    pgn_symbol = mov[0]
    pgn_2nd_symbol = mov[1]
    col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

    if (pgn_symbol == 'N') & (color == "white"):
        if kqw.new_position_posible(new_position, board_status, movements, num_movements, brd) & kkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            if pgn_2nd_symbol.isdigit():
                row1 = pm.get_position(2, brd)[0]
                row2 = pm.get_position(7, brd)[0]
                row_original = int(pgn_2nd_symbol) - 1
                if row1 == row_original:
                    piece_name = 'kqw'
                elif row2 == row_original:
                    piece_name = 'kkw'
            elif not pgn_2nd_symbol.isdigit():
                col1 = pm.get_position(2, brd)[1]
                col2 = pm.get_position(7, brd)[1]
                col_original = col_names.index(pgn_2nd_symbol)
                if col1 == col_original:
                    piece_name = 'kqw'
                elif col2 == col_original:
                    piece_name = 'kkw'
        elif kqw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'kqw'
        elif kkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'kkw'
        else:
            print("Movement illegal. No Knight available for this mevement")
    if (pgn_symbol == 'N') & (color == "black"):
        if kqb.new_position_posible(new_position, board_status, movements, num_movements, brd) & kkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            if pgn_2nd_symbol.isdigit():
                row1 = pm.get_position(26, brd)[0]
                row2 = pm.get_position(31, brd)[0]
                row_original = int(pgn_2nd_symbol) - 1
                if row1 == row_original:
                    piece_name = 'kqb'
                elif row2 == row_original:
                    piece_name = 'kkb'
            elif not pgn_2nd_symbol.isdigit():
                col1 = pm.get_position(26, brd)[1]
                col2 = pm.get_position(31, brd)[1]
                col_original = col_names.index(pgn_2nd_symbol)
                if col1 == col_original:
                    piece_name = 'kqb'
                elif col2 == col_original:
                    piece_name = 'kkb'
        elif kqb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'kqb'
        elif kkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'kkb'
        else:
            print("Movement illegal. No Knight available for this mevement")

    if (pgn_symbol == 'B') & (color == "white"):
        if bqw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'bqw'
        elif bkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'bkw'
        else:
            print("Movement illegal. No Bishop available for this mevement")
    if (pgn_symbol == 'B') & (color == "black"):
        if bqb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'bqb'
        elif bkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'bkb'
        else:
            print("Movement illegal. No Bishop available for this mevement")

    if (pgn_symbol == 'R') & (color == "white"):
        if rqw.new_position_posible(new_position, board_status, movements, num_movements, brd) & rkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            if pgn_2nd_symbol.isdigit():
                row1 = pm.get_position(1, brd)[0]
                row2 = pm.get_position(8, brd)[0]
                row_original = int(pgn_2nd_symbol) - 1
                if row1 == row_original:
                    piece_name = 'rqw'
                elif row2 == row_original:
                    piece_name = 'rkw'
            elif not pgn_2nd_symbol.isdigit():
                col1 = pm.get_position(1, brd)[1]
                col2 = pm.get_position(8, brd)[1]
                col_original = col_names.index(pgn_2nd_symbol)
                if col1 == col_original:
                    piece_name = 'rqw'
                elif col2 == col_original:
                    piece_name = 'rkw'
        elif rqw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'rqw'
        elif rkw.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'rkw'
        else:
            print("Movement illegal. No Rook available for this movement")
    if (pgn_symbol == 'R') & (color == "black"):
        if rqb.new_position_posible(new_position, board_status, movements, num_movements, brd) & rkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            if pgn_2nd_symbol.isdigit():
                row1 = pm.get_position(25, brd)[0]
                row2 = pm.get_position(32, brd)[0]
                row_original = int(pgn_2nd_symbol) - 1
                if row1 == row_original:
                    piece_name = 'rqb'
                elif row2 == row_original:
                    piece_name = 'rkb'
            elif not pgn_2nd_symbol.isdigit():
                col1 = pm.get_position(25, brd)[1]
                col2 = pm.get_position(32, brd)[1]
                col_original = col_names.index(pgn_2nd_symbol)
                if col1 == col_original:
                    piece_name = 'rqb'
                elif col2 == col_original:
                    piece_name = 'rkb'
        elif rqb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'rqb'
        elif rkb.new_position_posible(new_position, board_status, movements, num_movements, brd):
            piece_name = 'rkb'
        else:
            print("Movement illegal. No Rook available for this mevement")

    if (pgn_symbol == 'K') & (color == "white"):
        piece_name = 'kw'
    if (pgn_symbol == 'K') & (color == "black"):
        piece_name = 'kb'

    if (pgn_symbol == 'Q') & (color == "white"):
        # TODO: Disambiguation
        all_white_queens = ["qw", "qwa", "qwb",
                            "qwc", "qwd", "qwe", "qwf", "qwg", "qwh"]
        for queen in all_white_queens:
            if vars()[queen].new_position_posible(new_position, board_status, movements, num_movements, brd):
                piece_name = queen
    if (pgn_symbol == 'Q') & (color == "black"):
        all_black_queens = ["qb", "qba", "qbb",
                            "qbc", "qbd", "qbe", "qbf", "qbg", "qbh"]
        for queen in all_black_queens:
            if vars()[queen].new_position_posible(new_position, board_status, movements, num_movements, brd):
                piece_name = queen

    return(piece_name)


def get_all_pawns_in_col(col_letter, board_status, turn):
    col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    pawns_symbols = board_status[:, col_names.index(col_letter)]
    pawns_names = pm.convert_symbol_to_varnames(
        pawns_symbols[pawns_symbols != 0])
    if turn == "white":
        all_pawns = ['paw', 'pbw', 'pcw', 'pdw', 'pew', 'pfw', 'pgw',
                     'phw']
    else:
        all_pawns = ['pab', 'pbb', 'pcb', 'pdb', 'peb', 'pfb', 'pgb', 'phb']
    # pawn_names = all_pawns in list(pawns_names)
    pawn_names = [item for item in pawns_names if item in all_pawns]
    return(pawn_names)


def play(mov, halt_time, captured_pieces, num_movements, game_move, turn, brd, col_names, d, movements, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
         phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh):
    # init variables

    all_pieces = [rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                  phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb]
    all_pieces_args = [rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh]


    board_to_show = np.zeros((8, 8), dtype=np.int32)
    for i in range(0, 8):
        board_to_show[i, ] = brd.squares[7-i, ]
    prev_brd = board_to_show
    piece_to_promote = ""
    if num_movements % 2 == 0:
        row_king_castle = 0
        col_king_castle = 6
        row_queen_castle = 0
        col_queen_castle = 2
        letter = 'w'
        color = "white"
    else:
        row_king_castle = 7
        col_king_castle = 6
        row_queen_castle = 7
        col_queen_castle = 2
        letter = 'b'
        color = "black"
    if mov[0].islower():
        piece_type = "pawn"
        if mov[1] != 'x':
            new_row = int(mov[1]) - 1
            new_col = col_names.index(mov[0])
            new_position = [new_row, new_col]
            all_pawns_in_col = get_all_pawns_in_col(
                mov[0], brd.squares, turn)
            for pawn_name in all_pawns_in_col:
                if vars()[pawn_name].new_position_posible(new_position, brd.squares, movements, num_movements, brd):
                    piece_name = pawn_name
                else:
                    print('***** No pawn available for that movement!')
            is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                     brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
            prev_brd = gui.print_board_ascii(
                prev_brd, brd, halt_time, 0)
        elif mov[1] == 'x':
            new_row = int(mov[3]) - 1
            new_col = col_names.index(mov[2])
            new_position = [new_row, new_col]
            all_pawns_in_col = get_all_pawns_in_col(
                mov[0], brd.squares, turn)
            for pawn_name in all_pawns_in_col:
                if vars()[pawn_name].new_position_posible(new_position, brd.squares, movements, num_movements, brd):
                    piece_name = pawn_name
                else:
                    print('***** No pawn available for that movement!')
            is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                     brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
            prev_brd = gui.print_board_ascii(
                prev_brd, brd, halt_time, 0)

    elif not mov[0].islower():
        if (mov == 'O-O') or (mov == 'O-O+'):
            piece_type = "king"
            new_row = row_king_castle
            new_col = col_king_castle
            piece_name = "k" + letter
            new_position = [new_row, new_col]
            is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                     brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
            prev_brd = gui.print_board_ascii(
                prev_brd, brd, halt_time, 0)

        elif (mov == 'O-O-O') or (mov == 'O-O-O+'):
            piece_type = "king"
            new_row = row_queen_castle
            new_col = col_queen_castle
            piece_name = "k" + letter
            new_position = [new_row, new_col]
            is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                     brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
            prev_brd = gui.print_board_ascii(
                prev_brd, brd, halt_time, 0)

        else:
            piece_type = d[mov[0]][0]
            if mov[1] != 'x':
                if mov[2].isdigit():
                    new_row = int(mov[2]) - 1
                    new_col = col_names.index(mov[1])
                    new_position = [new_row, new_col]
                    piece_name = get_piece_name(
                        mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                        phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
                    is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                             brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
                    prev_brd = gui.print_board_ascii(
                        prev_brd, brd, halt_time, 0)
                elif not mov[2].isdigit():
                    if mov[2] != 'x':
                        new_row = int(mov[3]) - 1
                        new_col = col_names.index(mov[2])
                        new_position = [new_row, new_col]
                        piece_name = get_piece_name(
                            mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                            phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
                        is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
                        prev_brd = gui.print_board_ascii(
                            prev_brd, brd, halt_time, 0)

                    elif mov[2] == 'x':
                        new_row = int(mov[4]) - 1
                        new_col = col_names.index(mov[3])
                        new_position = [new_row, new_col]
                        piece_name = get_piece_name(
                            mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                            phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
                        is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
                        prev_brd = gui.print_board_ascii(
                            prev_brd, brd, halt_time, 0)

            elif mov[1] == 'x':
                new_row = int(mov[3]) - 1
                new_col = col_names.index(mov[2])
                new_position = [new_row, new_col]
                piece_name = get_piece_name(
                    mov, color, new_position, brd.squares, movements, num_movements, brd, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
                    phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh)
                is_castling_bool, castling_type, is_a_promotion_bool, promoted_piece, is_enpassant_bool, symbol_to_capture, movements,  end_game, num_movements, game_move,  turn, new_piece, brd,rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh = pm.move_piece(vars()[piece_name], new_position,
                                                                                                                                                                                                                                                                                                                                                                                                                                                         brd, all_pieces, num_movements, game_move, turn, captured_pieces, movements, mov, *all_pieces_args)
                prev_brd = gui.print_board_ascii(
                    prev_brd, brd, halt_time, 0)

    return mov, captured_pieces, num_movements, game_move, turn, brd, col_names, d, movements, rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw, phw, rqb, kqb, bqb, qb, kb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb, qwa, qwb, qwc, qwd, qwe, qwf, qwg, qwh, qba, qbb, qbc, qbd, qbe, qbf, qbg, qbh
