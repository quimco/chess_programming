import numpy as np
import board as br
import pieces_movement_functions as pm
import classes as cl
import gui as gui
import pandas as pd

rqw = cl.rook("rook_queen_white", [0, 0], "Active", "white", 1)
kqw = cl.knight("knight_queen_white", [0, 1], "Active", "white", 2)
bqw = cl.bishop("bishop_queen_white", [0, 2], "Active", "white", 3)
qw = cl.queen("queen_white", [0, 3], "Active", "white", 4)
kw = cl.king_white("king_white", [0, 5], "Active", "white", 5)
bkw = cl.bishop("bishop_king_white", [0, 5], "Active", "white", 6)
kkw = cl.knight("knight_king_white", [0, 6], "Active", "white", 7)
rkw = cl.rook("rook_king_white", [0, 7], "Active", "white", 8)

paw = cl.pawn("pawn_a_white", [6, 0], "Active", "white", 9)
pbw = cl.pawn("pawn_b_white", [1, 1], "Active", "white", 10)
pcw = cl.pawn("pawn_c_white", [1, 2], "Active", "white", 11)
pdw = cl.pawn("pawn_d_white", [1, 3], "Active", "white", 12)
pew = cl.pawn("pawn_e_white", [1, 4], "Active", "white", 13)
pfw = cl.pawn("pawn_f_white", [1, 5], "Active", "white", 14)
pgw = cl.pawn("pawn_g_white", [1, 6], "Active", "white", 15)
phw = cl.pawn("pawn_h_white", [1, 7], "Active", "white", 16)

rqb = cl.rook("rook_queen_black", [7, 0], "Active", "black", 25)
kqb = cl.knight("knight_queen_black", [7, 1], "Active", "black", 26)
bqb = cl.bishop("bishop_queen_black", [2, 2], "Active", "black", 27)
qb = cl.queen("queen_black", [7, 3], "Active", "black", 28)
kb = cl.king_black("king_black", [7, 4], "Active", "black", 29)
bkb = cl.bishop("bishop_king_black", [7, 5], "Active", "black", 30)
kkb = cl.knight("knight_king_black", [7, 6], "Active", "black", 31)
rkb = cl.rook("rook_king_black", [7, 7], "Active", "black", 32)

pab = cl.pawn("pawn_a_black", [6, 0], "Active", "black", 17)
pbb = cl.pawn("pawn_b_black", [6, 1], "Active", "black", 18)
pcb = cl.pawn("pawn_c_black", [6, 2], "Active", "black", 19)
pdb = cl.pawn("pawn_d_black", [6, 3], "Active", "black", 20)
peb = cl.pawn("pawn_e_black", [6, 4], "Active", "black", 21)
pfb = cl.pawn("pawn_f_black", [6, 5], "Active", "black", 22)
pgb = cl.pawn("pawn_g_black", [6, 6], "Active", "black", 23)
phb = cl.pawn("pawn_h_black", [6, 7], "Active", "black", 24)

captured_pieces = []
num_movements = 0
piece_to_promote = "queen"
all_pieces = [rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
              phw, rqb, kqb, bqb, qb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb]
turn = "white"
brd = br.board()
movements = pd.DataFrame(columns=[
                         "piece", "num_mov", "prev_row", "prev_col", "curr_row", "curr_col"], dtype=np.int8)

brd.squares = np.array([[1, 2, 3, 4, 5, 0, 0, 8], [9, 10, 11, 0, 13, 14, 15, 16], [0, 0, 27, 0, 0, 28, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [
                       0, 0, 0, 0, 0, 0, 0, 0], [17, 18, 19, 20, 21, 22, 23, 24], [25, 26, 0, 0, 29, 30, 31, 32]])

print(brd.squares)

wanted_pos = [1, 3]
piece_to_move = bqw
piece_to_promote = ""
print("**********************************")
print("Turn: ", turn)
print("Number of movements done: ", num_movements)
end_game, num_movements, turn, new_piece = pm.move_piece(piece_to_move, wanted_pos,
                                                         brd, all_pieces, num_movements, turn, captured_pieces, movements, piece_to_promote)
gui.print_board_ascii(brd)


captured_pieces = []
num_movements = 0
piece_to_promote = "queen"
all_pieces = [rqw, kqw, bqw, qw, kw, bkw, kkw, rkw, paw, pbw, pcw, pdw, pew, pfw, pgw,
              phw, rqb, kqb, bqb, qb, bkb, kkb, rkb, pab, pbb, pcb, pdb, peb, pfb, pgb, phb]
turn = "white"
brd = br.board()
movements = pd.DataFrame(columns=[
                         "piece", "num_mov", "prev_row", "prev_col", "curr_row", "curr_col"], dtype=np.int8)

brd.squares = np.array([[1, 2, 3, 4, 0, 5, 0, 8], [9, 10, 11, 0, 13, 14, 15, 16], [0, 0, 27, 0, 0, 28, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [
                       0, 0, 0, 0, 0, 0, 0, 0], [17, 18, 19, 20, 21, 22, 23, 24], [25, 26, 0, 0, 29, 30, 31, 32]])

print(brd.squares)

wanted_pos = [0, 4]
piece_to_move = kw
piece_to_promote = ""
print("**********************************")
print("Turn: ", turn)
print("Number of movements done: ", num_movements)
end_game, num_movements, turn, new_piece = pm.move_piece(piece_to_move, wanted_pos,
                                                         brd, all_pieces, num_movements, turn, captured_pieces, movements, piece_to_promote)
gui.print_board_ascii(brd)
