# Autor: Quim Coll
# Define all pieces as classes
# piece() is the main class to inherit from
# We have rook(), knight(), bishop(), queen, king_white, king_black and pawn that inherit from piece


import numpy as np
from pieces_movement_functions import square_attacked
from pieces_movement_functions import get_position


class piece():

    def __init__(self, name, initial_position, current_position, status, color, symbol):
        self.name = name
        self.current_position = current_position
        self.status = status
        self.color = color
        self.symbol = symbol
        self.initial_position = initial_position

    def get_color(self, symbol):
        if symbol in range(1, 17):
            return("white")
        elif symbol in range(17, 33):
            return("black")
        elif symbol in range(400, 700):
            return("white")
        elif symbol in range(700, 1000):
            return("black")

    def new_position(self, new_position, board_status, is_castling_bool, castling_type):
        if not is_castling_bool:
            self.old_position = self.current_position
            self.current_position = new_position
        elif is_castling_bool:
            print("Castling: ", castling_type)
            if castling_type == "king_white_king_side":
                self.old_position = self.current_position
                if self.name == "rook_king_white":
                    self.current_position = [0, 5]
                elif self.name == "king_white":
                    self.current_position = new_position
            elif castling_type == "king_white_queen_side":
                self.old_position = self.current_position
                if self.name == "rook_queen_white":
                    self.current_position = [0, 3]
                elif self.name == "king_white":
                    self.current_position = new_position
            elif castling_type == "king_black_king_side":
                self.old_position = self.current_position
                if self.name == "rook_king_black":
                    self.current_position = [7, 5]
                elif self.name == "king_black":
                    self.current_position = new_position
            elif castling_type == "king_black_queen_side":
                self.old_position = self.current_position
                if self.name == "rook_queen_black":
                    self.current_position = [7, 3]
                elif self.name == "king_black":
                    self.current_position = new_position
        return(self.old_position, self.current_position)

    def new_pos_avoids_check(self, turn, movements, num_movements, brd, new_position, is_enpassant_bool, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece):
        import copy

        aux_board = copy.deepcopy(brd)
        aux_board.update_board(self.current_position, new_position, turn,
                               is_enpassant_bool, self.symbol, castling_type, is_castling_bool, is_a_promotion_bool, promoted_piece)
        if turn == "white":
            king_symbol = 5
        else:
            king_symbol = 29
        pos_king = get_position(king_symbol, aux_board)
        king_in_check_bool = square_attacked(self.symbol,
                                             turn, pos_king, movements, num_movements, aux_board)
        return(not king_in_check_bool)

    @classmethod
    def position_within_board(cls, position):
        return((0 <= position[0] < 8) & (0 <= position[1] < 8))

    @classmethod
    def position_empty(cls, position, board_status):
        return(board_status[position[0], position[1]] == 0)


class rook(piece):

    def __init__(self, name, initial_position, current_position, status, color, symbol):
        super().__init__(name,  initial_position, current_position, status, color, symbol)

    def positions_inbetween(self, new_position, board_status):
        # Check if there are pieces between current and new position:
        is_horitzontal = new_position[0] == self.current_position[0]
        if is_horitzontal:
            # min and max in order range() to work properly
            first = min(new_position[1], self.current_position[1])
            second = max(new_position[1], self.current_position[1])
            output = True
            for i in range(first+1, second):
                if (board_status[self.current_position[0], i] != 0):
                    output = False
                    break
            return output
        else:
            # min and max in order range() to work properly
            first = min(new_position[0], self.current_position[0])
            second = max(new_position[0], self.current_position[0])
            output = True
            for i in range(first+1, second):
                if (board_status[i, self.current_position[1]] != 0):
                    output = False
                    break
            return output

    def new_position_posible(self, new_position, board_status, movements, num_movements, brd):
        # Check if movement can be done
        if self.status == "Active":
            # If one of two coordinates are equal, it means it's moving vertically or horitzontally correctly:
            if (new_position[0] == self.current_position[0]) or (
                    new_position[1] == self.current_position[1]):
                # Are there pieces inbetween two positions?
                if self.positions_inbetween(
                        new_position, board_status):
                    # Is the objective square empty or owned by a piece of different color?
                    if super().position_empty(new_position, board_status):
                        return(True)
                    else:
                        if super().get_color(board_status[new_position[0], new_position[1]]) != super(
                        ).get_color(board_status[self.current_position[0], self.current_position[1]]):
                            return(True)
                        return(False)
                return(False)
            return(False)
        return(False)


class knight(piece):
    def __init__(self, name, initial_position, current_position, status, color, symbol):
        super().__init__(name,  initial_position, current_position, status, color, symbol)

    def new_position_posible(self, new_position, board_status, movements, num_movements, brd):
        # Check if movement can be done
        if self.status == "Active":
            # Define Knight movements:
            # Available movements:
            self.available_coord = [[1, 2], [2, 1], [-1, 2],
                                    [-2, 1], [1, -2], [2, -1], [-1, -2], [-2, -1]]
            self.knight_movement_bool = False
            for coord in self.available_coord:
                if (new_position[0] == self.current_position[0] + coord[0]) & (new_position[1] == self.current_position[1] + coord[1]):
                    self.knight_movement_bool = True
                    break
            if self.knight_movement_bool:
                # Is the objective square empty or owned by a piece of different color?
                if super().position_empty(new_position, board_status):
                    return True
                else:
                    if super().get_color(board_status[new_position[0], new_position[1]]) != super().get_color(board_status[self.current_position[0], self.current_position[1]]):
                        return True
                    return False
            return False
        return False


class bishop(piece):

    def __init__(self, name, initial_position, current_position, status, color, symbol):
        super().__init__(name,  initial_position, current_position, status, color, symbol)

    def positions_inbetween(self, new_position, board_status):
        all_x = []
        stepx = 1 if new_position[0] > self.current_position[0] else -1
        for i in range(self.current_position[0] + stepx, new_position[0], stepx):
            all_x.append(i)

        all_y = []
        stepy = 1 if new_position[1] > self.current_position[1] else -1

        for j in range(self.current_position[1] + stepy, new_position[1], stepy):
            all_y.append(j)

        bool_seq = True
        for k in range(0, len(all_x)):
            if (board_status[all_x[k], all_y[k]] != 0):
                bool_seq = False
                break

        return(bool_seq)

    def new_position_posible(self, new_position, board_status, movements, num_movements, brd):
        # Check if movement can be done
        if self.status == "Active":
            # Dist in x == Dist in y, in absolute.
            if abs(new_position[0] - self.current_position[0]) == abs(new_position[1] - self.current_position[1]):
                # Are there pieces inbetween two positions?
                if self.positions_inbetween(new_position, board_status):
                    # Is the objective square empty or owned by a piece of different color?
                    if super().position_empty(new_position, board_status):
                        return True
                    else:
                        if super().get_color(board_status[new_position[0], new_position[1]]) != super().get_color(board_status[self.current_position[0], self.current_position[1]]):
                            return True
                        return False
                return False
            return False
        return False


class queen(piece):

    def __init__(self, name, initial_position, current_position, status, color, symbol):
        super().__init__(name,  initial_position, current_position, status, color, symbol)

    def positions_inbetween(self, new_position, board_status):
        # Check if there are pieces between current and new position:
        # First we check if it moves like a rook or like a bishop , and apply its algorithm:
        self.is_rook_like_move = (new_position[0] == self.current_position[0]) or (
            new_position[1] == self.current_position[1])
        if self.is_rook_like_move:
            is_horitzontal = new_position[0] == self.current_position[0]
            if is_horitzontal:
                first = min(new_position[1], self.current_position[1])
                second = max(new_position[1], self.current_position[1])

                output = True
                for i in range(first+1, second):
                    if (board_status[self.current_position[0], i] != 0):
                        output = False
                        break
                return output

            else:
                first = min(new_position[0], self.current_position[0])
                second = max(new_position[0], self.current_position[0])
                output = True
                for i in range(first+1, second):
                    if (board_status[i, self.current_position[1]] != 0):
                        output = False
                        break
                return output

        else:
            all_x = []
            stepx = 1 if new_position[0] > self.current_position[0] else -1
            for i in range(self.current_position[0] + stepx, new_position[0], stepx):
                all_x.append(i)

            all_y = []
            stepy = 1 if new_position[1] > self.current_position[1] else -1

            for j in range(self.current_position[1] + stepy, new_position[1], stepy):
                all_y.append(j)

            bool_seq = True
            for k in range(0, len(all_x)):
                if (board_status[all_x[k], all_y[k]] != 0):
                    bool_seq = False
                    break

            return(bool_seq)

    def new_position_posible(self, new_position, board_status, movements, num_movements, brd):
        # Check if movement can be done
        if self.status == "Active":
            # Rook: If one of two coordinates are equal, it means it's moving vertically or horitzontally correctly:
            self.queen_rook_like_movement_bool = (new_position[0] == self.current_position[0]) or (
                new_position[1] == self.current_position[1])
            # Bishop:  Dist in x == Dist in y, in absolute.
            self.queen_bishop_like_movement_bool = abs(
                new_position[0] - self.current_position[0]) == abs(new_position[1] - self.current_position[1])
            # move ok if Rook OR Bishop like movement:
            self.queen_movement_bool = self.queen_rook_like_movement_bool or self.queen_bishop_like_movement_bool
            if self.queen_movement_bool:
                # Are there pieces inbetween two positions?
                if self.positions_inbetween(new_position, board_status):
                    # Is the objective square empty or owned by a piece of different color?
                    if super().position_empty(new_position, board_status):
                        return True
                    else:
                        if super().get_color(board_status[new_position[0], new_position[1]]) != super().get_color(board_status[self.current_position[0], self.current_position[1]]):
                            return True
                        return False
                return False
            return False
        return False


class king_white(piece):
    from pieces_movement_functions import square_attacked

    def __init__(self, name, initial_position, current_position, status, color, symbol):
        super().__init__(name,  initial_position, current_position, status, color, symbol)

    def is_castling(self, new_position, board_status, movements, num_movements, brd):
        if (self.symbol == 5) & (new_position[1] == 6):
            self.castling_type = "king_white_king_side"
            # Rule 3: Rook exists (is active)
            if (8 in board_status):
                # Rule 4: King and rook path is free
                if (board_status[0, 5] == 0) & (board_status[0, 6] == 0):
                    # Rule 1: King has not previous moves:
                    if not (self.symbol in movements["piece"].values):
                        # Rule 2: Rook has not previous moves:
                        if not (8 in movements["piece"].values):
                            # Rule 6: King is not in check
                            if not square_attacked(self.symbol, self.color, [0, 4], movements, num_movements, brd):
                                # Rule 5: King path is not attacked
                                if (not square_attacked(self.symbol, self.color, [0, 5], movements, num_movements, brd)) & (not square_attacked(self.symbol, self.color, [0, 6], movements, num_movements, brd)):
                                    return (self.castling_type, True)
            return (self.castling_type, False)

        elif (self.symbol == 5) & (new_position[1] == 2):
            self.castling_type = "king_white_queen_side"
            # Rule 3: Rook exists (is active)
            if (1 in board_status):
                # Rule 4: King and rook path is free
                if (board_status[0, 1] == 0) & (board_status[0, 2] == 0) & (board_status[0, 3] == 0):
                    # Rule 1: King has not previous moves:
                    if not (self.symbol in movements["piece"].values):
                        # Rule 2: Rook has not previous moves:
                        if not (1 in movements["piece"].values):
                            # Rule 6: King is not in check
                            if not square_attacked(self.symbol, self.color, [0, 4], movements, num_movements, brd):
                                # Rule 5: King path is not attacked
                                if (not square_attacked(self.symbol, self.color, [0, 2], movements, num_movements, brd)) & (not square_attacked(self.symbol, self.color, [0, 3], movements, num_movements, brd)):
                                    return (self.castling_type, True)
            return (self.castling_type, False)

        return("", False)

    def new_position_posible(self, new_position, board_status, movements, num_movements, brd):
        # Check iw movement can be done
        if self.status == "Active":
            self.trying_castling_bool = (new_position[0] - self.current_position[0] == 0) & (abs(
                new_position[1] - self.current_position[1]) == 2)  # movement is lateral and dist == 2
            if not self.trying_castling_bool:
                if (abs(new_position[0] - self.current_position[0]) in [0, 1]) and (abs(new_position[1] - self.current_position[1]) in [0, 1]):
                    # Is the objective square empty or owned by a piece of different color?
                    if super().position_empty(new_position, board_status):
                        if not square_attacked(self.symbol, self.color, new_position, movements, num_movements, brd):
                            return True
                    else:
                        if super().get_color(board_status[new_position[0], new_position[1]]) != super().get_color(board_status[self.current_position[0], self.current_position[1]]):
                            if not square_attacked(self.symbol, self.color, new_position, movements, num_movements, brd):
                                return True
                        return False
                    return False
                return False
            elif self.trying_castling_bool:
                __, is_castling_bool = self.is_castling(
                    new_position, board_status, movements, num_movements, brd)
                return is_castling_bool
        return False


class king_black(piece):
    from pieces_movement_functions import square_attacked

    def __init__(self, name, initial_position, current_position, status, color, symbol):
        super().__init__(name,  initial_position, current_position, status, color, symbol)

    def is_castling(self, new_position, board_status, movements, num_movements, brd):
        if (self.symbol == 29) & (new_position[1] == 6):
            self.castling_type = "king_black_king_side"
            # Rule 3: Rook exists (is active)
            if (32 in board_status):
                # Rule 4: King and rook path is free
                if (board_status[7, 5] == 0) & (board_status[7, 6] == 0):
                    # Rule 1: King has not previous moves:
                    if not (self.symbol in movements["piece"].values):
                        # Rule 2: Rook has not previous moves:
                        if not (32 in movements["piece"].values):
                            # Rule 6: King is not in check
                            if not square_attacked(self.symbol, self.color, [7, 4], movements, num_movements, brd):
                                # Rule 5: King path is not attacked
                                if (not square_attacked(self.symbol, self.color, [7, 5], movements, num_movements, brd)) & (not square_attacked(self.symbol, self.color, [7, 6], movements, num_movements, brd)):
                                    return (self.castling_type, True)
            return (self.castling_type, False)

        elif (self.symbol == 29) & (new_position[1] == 2):
            self.castling_type = "king_black_queen_side"
            # Rule 3: Rook exists (is active)
            if (25 in board_status):
                # Rule 4: King and rook path is free
                if (board_status[7, 1] == 0) & (board_status[7, 2] == 0) & (board_status[7, 3] == 0):
                    # Rule 1: King has not previous moves:
                    if not (self.symbol in movements["piece"].values):
                        # Rule 2: Rook has not previous moves:
                        if not (25 in movements["piece"].values):
                            # Rule 6: King is not in check
                            if not square_attacked(self.symbol, self.color, [7, 4], movements, num_movements, brd):
                                # Rule 5: King path is not attacked
                                if (not square_attacked(self.symbol, self.color, [7, 2], movements, num_movements, brd)) & (not square_attacked(self.symbol, self.color, [7, 3], movements, num_movements, brd)):
                                    return (self.castling_type, True)
            return (self.castling_type, False)
        return("", False)

    def new_position_posible(self, new_position, board_status, movements, num_movements, brd):
        # Check if movement can be done
        if self.status == "Active":
            self.trying_castling_bool = (new_position[0] - self.current_position[0] == 0) & (abs(
                new_position[1] - self.current_position[1]) == 2)  # movement is lateral and dist == 2
            if not self.trying_castling_bool:
                if (abs(new_position[0] - self.current_position[0]) in [0, 1]) and (abs(new_position[1] - self.current_position[1]) in [0, 1]):
                    # Is the objective square empty or owned by a piece of different color?
                    if super().position_empty(new_position, board_status):
                        if not square_attacked(self.symbol, self.color, new_position, movements, num_movements, brd):
                            return True
                    else:
                        if super().get_color(board_status[new_position[0], new_position[1]]) != super().get_color(board_status[self.current_position[0], self.current_position[1]]):
                            if not square_attacked(self.symbol, self.color, new_position, movements, num_movements, brd):
                                return True
                        return False
                    return False
                return False
            elif self.trying_castling_bool:
                __, is_castling_bool = self.is_castling(
                    new_position, board_status, movements, num_movements, brd)
                return is_castling_bool
        return False


class pawn(piece):

    def __init__(self, name, initial_position, current_position, status, color, symbol):
        super().__init__(name,  initial_position, current_position, status, color, symbol)

    def is_enpassant(self, new_position, board_status, movements, num_movements):
        if self.color == "white":
            # pawn in row 4
            if self.current_position[0] == 4:
                # check if it's trying to capture:
                if (new_position[0] == self.current_position[0] + 1) & ((new_position[1] == self.current_position[1] + 1) or (new_position[1] == self.current_position[1] - 1)):
                    if super().position_empty(new_position, board_status):
                        if movements.loc[num_movements, "piece"] in range(17, 25):
                            if movements.loc[num_movements, "curr_row"] - movements.loc[num_movements, "prev_row"] == -2:
                                if movements.loc[num_movements, "curr_col"] == new_position[1]:
                                    return True
                                return False
                            return False
                        return False
                    return False
                return False
            return False
        elif self.color == 'black':
            if self.current_position[0] == 3:
                # check if it's trying to capture:
                if (new_position[0] == self.current_position[0] - 1) & ((new_position[1] == self.current_position[1] + 1) or (new_position[1] == self.current_position[1] - 1)):
                    if super().position_empty(new_position, board_status):
                        if movements.loc[num_movements, "piece"] in range(9, 17):
                            if movements.loc[num_movements, "curr_row"] - movements.loc[num_movements, "prev_row"] == 2:
                                if movements.loc[num_movements, "curr_col"] == new_position[1]:
                                    return True
                                return False
                            return False
                        return False
                    return False
                return False
            return False

    def is_attacking(self, new_position):
        if self.status == "Active":
            if self.color == "white":
                # check if it's trying to capture:
                self.is_trying_capture_bool = (new_position[0] == self.current_position[0] + 1) & (
                    (new_position[1] == self.current_position[1] + 1) or (new_position[1] == self.current_position[1] - 1))
                return self.is_trying_capture_bool
            elif self.color == 'black':
                # check if it's trying to capture:
                self.is_trying_capture_bool = (new_position[0] == self.current_position[0] - 1) & (
                    (new_position[1] == self.current_position[1] + 1) or (new_position[1] == self.current_position[1] - 1))
                return self.is_trying_capture_bool

    def new_position_posible(self, new_position, board_status, movements, num_movements, brd):
        # Check iw movement can be done
        if self.status == "Active":
            if self.color == "white":
                # check if it's trying to capture:
                self.is_trying_capture_bool = (new_position[0] == self.current_position[0] + 1) & (
                    (new_position[1] == self.current_position[1] + 1) or (new_position[1] == self.current_position[1] - 1))
                if self.is_trying_capture_bool:
                    if super().position_empty(new_position, board_status):
                        # if new position is empty, capture  possible only if is enpassant:
                        return self.is_enpassant(new_position, board_status, movements, num_movements)
                    else:
                        # capture only possible if objective square is occupied by dif color piece:
                        if super().get_color(board_status[new_position[0], new_position[1]]) != super().get_color(board_status[self.current_position[0], self.current_position[1]]):
                            return True
                        return False

                else:  # if it is not trying to capture:
                    # move only in same column:
                    if self.current_position[1] == new_position[1]:
                        # it's opening:
                        if self.current_position[0] == 1:
                            # can only move 1 or 2:
                            if new_position[0] - self.current_position[0] == 2:
                                self.pos_to_check = [
                                    new_position[0] - 1, new_position[1]]
                                # if opens in 2,  square  between is empty
                                if super().position_empty(self.pos_to_check, board_status):
                                    # objective position it's empty:
                                    if super().position_empty(new_position, board_status):
                                        return True
                                    return False
                                return False
                            elif new_position[0] - self.current_position[0] == 1:
                                if super().position_empty(new_position, board_status):
                                    return True
                                return False
                            else:
                                return False
                            return False
                        elif self.current_position[0] != 1:
                            if (new_position[0] - self.current_position[0]) == 1:
                                if super().position_empty(new_position, board_status):
                                    return True
                                return False
                            return False
                    return False
            elif self.color == 'black':
                # check if it's trying to capture:
                self.is_trying_capture_bool = (new_position[0] == self.current_position[0] - 1) & (
                    (new_position[1] == self.current_position[1] + 1) or (new_position[1] == self.current_position[1] - 1))
                if self.is_trying_capture_bool:
                    if super().position_empty(new_position, board_status):
                        # if new position is empty, capture  possible only if is enpassant:
                        return self.is_enpassant(new_position, board_status, movements, num_movements)
                    else:
                        # capture only possible if objective square is occupied by dif color piece:
                        if super().get_color(board_status[new_position[0], new_position[1]]) != super().get_color(board_status[self.current_position[0], self.current_position[1]]):
                            return True
                        return False

                else:  # if it is not trying to capture:
                    # move only in same column:
                    if self.current_position[1] == new_position[1]:
                        # it's opening:
                        if self.current_position[0] == 6:
                            # can only move 1 or 2:
                            if new_position[0] - self.current_position[0] == - 2:
                                self.pos_to_check = [
                                    new_position[0] + 1, new_position[1]]
                                # if opens in 2,  square  between is empty
                                if super().position_empty(self.pos_to_check, board_status):
                                    # objective position it's empty:
                                    if super().position_empty(new_position, board_status):
                                        return True
                                    return False
                                return False
                            elif new_position[0] - self.current_position[0] == - 1:
                                if super().position_empty(new_position, board_status):
                                    return True
                                return False
                            return False
                        elif self.current_position[0] != 6:
                            if (new_position[0] - self.current_position[0]) == - 1:
                                if super().position_empty(new_position, board_status):
                                    return True
                                return False
                            return False
                    return False
        return False
