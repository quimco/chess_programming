# Autor: Quim Coll
# Configuration file for play_framework
# Initializes all varibales, including pieces
# Includes 2 test positions, to start in a different configuration if wanted

import numpy as np
import pandas as pd
import classes as cl
import board as br
import pieces_movement_functions as pm
from copy import copy
from copy import deepcopy


## Initialize all variables

col_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
d = dict({'K': ['king', 'k'], 'Q': ['queen', 'q'], 'B': [
    'bishop', 'b'], 'N': ['knight', 'k'], 'R': ['rook', 'r']})
movements = pd.DataFrame(columns=[
    "piece", "num_mov", "prev_row", "prev_col", "curr_row", "curr_col", "pgn_movement", "game_num_move"])
piece_to_promote = ""
captured_pieces = []
num_movements = 0
game_move = 0
turn = "white"
end_game = False
halt_time = 0
depth = 1
all_pieces = []
promoted_piece = ""
pgn_mov = "pgn_mov"
piece = ""
piece_to_move = []
best_move = []
move = ""
piece = ""
brd = br.board()
initial_board = deepcopy(brd)

## Define 2 test postiions to begin, different from default

test_position1 = False
test_position2 = False

if test_position1:

    brd.squares = np.array([[0, 0, 0, 0, 5, 0, 0, 0, ], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [
        0, 0, 0, 0, 0, 0, 0, 0], [0, 6, 0, 0, 0, 0, 0, 0], [26, 0, 27, 0, 0, 0, 0, 0], [25, 0, 0, 0, 29, 0, 0, 0]], dtype=np.int8)
    rqw = cl.rook("rook_queen_white", [0, 0], [0, 0], "Active", "white", 1)
    kqw = cl.knight("knight_queen_white", [0, 1], [0, 1], "Active", "white", 2)
    bqw = cl.bishop("bishop_queen_white", [0, 2], [0, 2], "Active", "white", 3)
    qw = cl.queen("queen_white", [0, 3], [0, 3], "Active", "white", 4)
    kw = cl.king_white("king_white", [0, 4], [0, 4], "Active", "white", 5)

    #bkw = cl.bishop("bishop_king_white", [0, 5], [0, 5], "Active", "white", 6)
    bkw = cl.bishop("bishop_king_white", [5, 1], [5, 1], "Active", "white", 6)

    kkw = cl.knight("knight_king_white", [0, 6], [0, 6], "Active", "white", 7)
    rkw = cl.rook("rook_king_white", [0, 7], [0, 7], "Active", "white", 8)

    paw = cl.pawn("pawn_a_white", [1, 0], [1, 0], "Active", "white", 9)
    pbw = cl.pawn("pawn_b_white", [1, 1], [1, 1], "Active", "white", 10)
    pcw = cl.pawn("pawn_c_white", [1, 2], [1, 2], "Active", "white", 11)
    pdw = cl.pawn("pawn_d_white", [1, 2], [1, 2], "Active", "white", 12)
    pew = cl.pawn("pawn_e_white", [1, 4], [1, 4], "Active", "white", 13)
    pfw = cl.pawn("pawn_f_white", [1, 5], [1, 5], "Active", "white", 14)
    pgw = cl.pawn("pawn_g_white", [1, 6], [1, 6], "Active", "white", 15)
    phw = cl.pawn("pawn_h_white", [1, 7], [1, 7], "Active", "white", 16)

    rqb = cl.rook("rook_queen_black", [7, 0], [7, 0], "Active", "black", 25)
    # kqb = cl.knight("knight_queen_black", [7, 1], [
    #                 7, 1], "Active", "black", 26)
    kqb = cl.knight("knight_queen_black", [6, 0], [
                    6, 0], "Active", "black", 26)

    # bqb = cl.bishop("bishop_queen_black", [7, 2], [
    #                7, 2], "Active", "black", 27)
    bqb = cl.bishop("bishop_queen_black", [6, 2], [
                    6, 2], "Active", "black", 27)

    qb = cl.queen("queen_black", [7, 3], [7, 3], "Active", "black", 28)
    kb = cl.king_black("king_black", [7, 4], [7, 4], "Active", "black", 29)
    bkb = cl.bishop("bishop_king_black", [7, 5], [7, 5], "Active", "black", 30)
    kkb = cl.knight("knight_king_black", [7, 6], [7, 6], "Active", "black", 31)
    rkb = cl.rook("rook_king_black", [7, 7], [7, 7], "Active", "black", 32)

    pab = cl.pawn("pawn_a_black", [6, 0], [6, 0], "Active", "black", 17)
    pbb = cl.pawn("pawn_b_black", [6, 1], [6, 1], "Active", "black", 18)
    pcb = cl.pawn("pawn_c_black", [6, 2], [6, 2], "Active", "black", 19)
    pdb = cl.pawn("pawn_d_black", [6, 3], [6, 3], "Active", "black", 20)
    peb = cl.pawn("pawn_e_black", [6, 4], [6, 4], "Active", "black", 21)
    pfb = cl.pawn("pawn_f_black", [6, 5], [6, 5], "Active", "black", 22)
    pgb = cl.pawn("pawn_g_black", [6, 6], [6, 6], "Active", "black", 23)
    phb = cl.pawn("pawn_h_black", [6, 7], [6, 7], "Active", "black", 24)

    qwa = cl.queen("queen_white_a", [7, 0], [7, 0], "Inactive", "white", 409)
    qwb = cl.queen("queen_white_b", [7, 1], [7, 1], "Inactive", "white", 410)
    qwc = cl.queen("queen_white_c", [7, 2], [7, 2], "Inactive", "white", 411)
    qwd = cl.queen("queen_white_d", [7, 3], [7, 3], "Inactive", "white", 412)
    qwe = cl.queen("queen_white_e", [7, 4], [7, 4], "Inactive", "white", 413)
    qwf = cl.queen("queen_white_f", [7, 5], [7, 5], "Inactive", "white", 414)
    qwg = cl.queen("queen_white_g", [7, 6], [7, 6], "Inactive", "white", 415)
    qwh = cl.queen("queen_white_h", [7, 7], [7, 7], "Inactive", "white", 416)

    qba = cl.queen("queen_black_a", [0, 0], [0, 0], "Inactive", "black", 717)
    qbb = cl.queen("queen_black_b", [0, 1], [0, 1], "Inactive", "black", 718)
    qbc = cl.queen("queen_black_c", [0, 2], [0, 2], "Inactive", "black", 719)
    qbd = cl.queen("queen_black_d", [0, 3], [0, 3], "Inactive", "black", 720)
    qbe = cl.queen("queen_black_e", [0, 4], [0, 4], "Inactive", "black", 721)
    qbf = cl.queen("queen_black_f", [0, 5], [0, 5], "Inactive", "black", 722)
    qbg = cl.queen("queen_black_g", [0, 6], [0, 6], "Inactive", "black", 723)
    qbh = cl.queen("queen_black_h", [0, 7], [0, 7], "Inactive", "black", 724)


elif test_position2:

    brd.squares = np.array([[1, 2, 3, 4, 5, 0, 0, 8], [9, 10, 11, 12, 6, 14, 15, 16], [0, 0, 0, 0, 0, 7, 0, 0], [0, 0, 0, 0, 21, 0, 0, 0], [0, 0, 0, 13, 0, 0, 0, 0], [
        0, 0, 0, 0, 0, 28, 0, 0], [17, 18, 19, 0, 0, 22, 23, 24], [25, 26, 27, 0, 29, 30, 31, 32]], dtype=np.int32)
    rqw = cl.rook("rook_queen_white", [0, 0], [0, 0], "Active", "white", 1)
    kqw = cl.knight("knight_queen_white", [0, 1], [0, 1], "Active", "white", 2)
    bqw = cl.bishop("bishop_queen_white", [0, 2], [0, 2], "Active", "white", 3)
    qw = cl.queen("queen_white", [0, 3], [0, 3], "Active", "white", 4)
    kw = cl.king_white("king_white", [0, 4], [0, 4], "Active", "white", 5)

    bkw = cl.bishop("bishop_king_white", [1, 4], [1, 4], "Active", "white", 6)

    kkw = cl.knight("knight_king_white", [2, 5], [2, 5], "Active", "white", 7)
    rkw = cl.rook("rook_king_white", [0, 7], [0, 7], "Active", "white", 8)

    paw = cl.pawn("pawn_a_white", [1, 0], [1, 0], "Active", "white", 9)
    pbw = cl.pawn("pawn_b_white", [1, 1], [1, 1], "Active", "white", 10)
    pcw = cl.pawn("pawn_c_white", [1, 2], [1, 2], "Active", "white", 11)
    pdw = cl.pawn("pawn_d_white", [1, 3], [1, 3], "Active", "white", 12)
    pew = cl.pawn("pawn_e_white", [4, 3], [4, 3], "Active", "white", 13)
    pfw = cl.pawn("pawn_f_white", [1, 5], [1, 5], "Active", "white", 14)
    pgw = cl.pawn("pawn_g_white", [1, 6], [1, 6], "Active", "white", 15)
    phw = cl.pawn("pawn_h_white", [1, 7], [1, 7], "Active", "white", 16)

    rqb = cl.rook("rook_queen_black", [7, 0], [7, 0], "Active", "black", 25)
    kqb = cl.knight("knight_queen_black", [6, 0], [
                    6, 0], "Active", "black", 26)
    bqb = cl.bishop("bishop_queen_black", [6, 2], [
                    6, 2], "Active", "black", 27)

    qb = cl.queen("queen_black", [5, 5], [5, 5], "Active", "black", 28)
    kb = cl.king_black("king_black", [7, 4], [7, 4], "Active", "black", 29)
    bkb = cl.bishop("bishop_king_black", [7, 5], [7, 5], "Active", "black", 30)
    kkb = cl.knight("knight_king_black", [7, 6], [7, 6], "Active", "black", 31)
    rkb = cl.rook("rook_king_black", [7, 7], [7, 7], "Active", "black", 32)

    pab = cl.pawn("pawn_a_black", [6, 0], [6, 0], "Active", "black", 17)
    pbb = cl.pawn("pawn_b_black", [6, 1], [6, 1], "Active", "black", 18)
    pcb = cl.pawn("pawn_c_black", [6, 2], [6, 2], "Active", "black", 19)
    pdb = cl.pawn("pawn_d_black", [6, 3], [6, 3], "Inactive", "black", 20)
    peb = cl.pawn("pawn_e_black", [3, 4], [3, 4], "Active", "black", 21)
    pfb = cl.pawn("pawn_f_black", [6, 5], [6, 5], "Active", "black", 22)
    pgb = cl.pawn("pawn_g_black", [6, 6], [6, 6], "Active", "black", 23)
    phb = cl.pawn("pawn_h_black", [6, 7], [6, 7], "Active", "black", 24)

    qwa = cl.queen("queen_white_a", [7, 0], [7, 0], "Inactive", "white", 409)
    qwb = cl.queen("queen_white_b", [7, 1], [7, 1], "Inactive", "white", 410)
    qwc = cl.queen("queen_white_c", [7, 2], [7, 2], "Inactive", "white", 411)
    qwd = cl.queen("queen_white_d", [7, 3], [7, 3], "Inactive", "white", 412)
    qwe = cl.queen("queen_white_e", [7, 4], [7, 4], "Inactive", "white", 413)
    qwf = cl.queen("queen_white_f", [7, 5], [7, 5], "Inactive", "white", 414)
    qwg = cl.queen("queen_white_g", [7, 6], [7, 6], "Inactive", "white", 415)
    qwh = cl.queen("queen_white_h", [7, 7], [7, 7], "Inactive", "white", 416)

    qba = cl.queen("queen_black_a", [0, 0], [0, 0], "Inactive", "black", 717)
    qbb = cl.queen("queen_black_b", [0, 1], [0, 1], "Inactive", "black", 718)
    qbc = cl.queen("queen_black_c", [0, 2], [0, 2], "Inactive", "black", 719)
    qbd = cl.queen("queen_black_d", [0, 3], [0, 3], "Inactive", "black", 720)
    qbe = cl.queen("queen_black_e", [0, 4], [0, 4], "Inactive", "black", 721)
    qbf = cl.queen("queen_black_f", [0, 5], [0, 5], "Inactive", "black", 722)
    qbg = cl.queen("queen_black_g", [0, 6], [0, 6], "Inactive", "black", 723)
    qbh = cl.queen("queen_black_h", [0, 7], [0, 7], "Inactive", "black", 724)


## default position: standard chess

else:

    brd.squares = np.array([[1, 2, 3, 4, 5, 6, 7, 8], [9, 10, 11, 12, 13, 14, 15, 16], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [
        0, 0, 0, 0, 0, 0, 0, 0], [17, 18, 19, 20, 21, 22, 23, 24], [25, 26, 27, 28, 29, 30, 31, 32]], dtype=np.int32)

    rqw = cl.rook("rook_queen_white", pm.get_position(1, brd),
                  pm.get_position(1, brd), "Active", "white", 1)
    kqw = cl.knight("knight_queen_white", pm.get_position(2, brd),
                    pm.get_position(2, brd), "Active", "white", 2)
    bqw = cl.bishop("bishop_queen_white", pm.get_position(3, brd),
                    pm.get_position(3, brd), "Active", "white", 3)
    qw = cl.queen("queen_white", pm.get_position(4, brd),
                  pm.get_position(4, brd), "Active", "white", 4)
    kw = cl.king_white("king_white", pm.get_position(5, brd),
                       pm.get_position(5, brd), "Active", "white", 5)
    bkw = cl.bishop("bishop_king_white", pm.get_position(6, brd),
                    pm.get_position(6, brd), "Active", "white", 6)
    kkw = cl.knight("knight_king_white", pm.get_position(7, brd),
                    pm.get_position(7, brd), "Active", "white", 7)
    rkw = cl.rook("rook_king_white", pm.get_position(8, brd),
                  pm.get_position(8, brd), "Active", "white", 8)

    paw = cl.pawn("pawn_a_white", pm.get_position(9, brd),
                  pm.get_position(9, brd), "Active", "white", 9)
    pbw = cl.pawn("pawn_b_white", pm.get_position(10, brd),
                  pm.get_position(10, brd), "Active", "white", 10)
    pcw = cl.pawn("pawn_c_white", pm.get_position(11, brd),
                  pm.get_position(11, brd), "Active", "white", 11)
    pdw = cl.pawn("pawn_d_white", pm.get_position(12, brd),
                  pm.get_position(12, brd), "Active", "white", 12)
    pew = cl.pawn("pawn_e_white", pm.get_position(13, brd),
                  pm.get_position(13, brd), "Active", "white", 13)
    pfw = cl.pawn("pawn_f_white", pm.get_position(14, brd),
                  pm.get_position(14, brd), "Active", "white", 14)
    pgw = cl.pawn("pawn_g_white", pm.get_position(15, brd),
                  pm.get_position(15, brd), "Active", "white", 15)
    phw = cl.pawn("pawn_h_white", pm.get_position(16, brd),
                  pm.get_position(16, brd), "Active", "white", 16)

    rqb = cl.rook("rook_queen_black", pm.get_position(25, brd),
                  pm.get_position(25, brd), "Active", "black", 25)
    kqb = cl.knight("knight_queen_black", pm.get_position(26, brd),
                    pm.get_position(26, brd), "Active", "black", 26)
    bqb = cl.bishop("bishop_queen_black", pm.get_position(27, brd),
                    pm.get_position(27, brd), "Active", "black", 27)
    qb = cl.queen("queen_black", pm.get_position(28, brd),
                  pm.get_position(28, brd), "Active", "black", 28)
    kb = cl.king_black("king_black", pm.get_position(29, brd),
                       pm.get_position(29, brd), "Active", "black", 29)
    bkb = cl.bishop("bishop_king_black", pm.get_position(30, brd),
                    pm.get_position(30, brd), "Active", "black", 30)
    kkb = cl.knight("knight_king_black", pm.get_position(31, brd),
                    pm.get_position(31, brd), "Active", "black", 31)
    rkb = cl.rook("rook_king_black", pm.get_position(32, brd),
                  pm.get_position(32, brd), "Active", "black", 32)

    pab = cl.pawn("pawn_a_black", pm.get_position(17, brd),
                  pm.get_position(17, brd), "Active", "black", 17)
    pbb = cl.pawn("pawn_b_black", pm.get_position(18, brd),
                  pm.get_position(18, brd), "Active", "black", 18)
    pcb = cl.pawn("pawn_c_black", pm.get_position(19, brd),
                  pm.get_position(19, brd), "Active", "black", 19)
    pdb = cl.pawn("pawn_d_black", pm.get_position(20, brd),
                  pm.get_position(20, brd), "Active", "black", 20)
    peb = cl.pawn("pawn_e_black", pm.get_position(21, brd),
                  pm.get_position(21, brd), "Active", "black", 21)
    pfb = cl.pawn("pawn_f_black", pm.get_position(22, brd),
                  pm.get_position(22, brd), "Active", "black", 22)
    pgb = cl.pawn("pawn_g_black", pm.get_position(23, brd),
                  pm.get_position(23, brd), "Active", "black", 23)
    phb = cl.pawn("pawn_h_black", pm.get_position(24, brd),
                  pm.get_position(24, brd), "Active", "black", 24)

    qwa = cl.queen("queen_white_a", [7, 0], [7, 0], "Inactive", "white", 409)
    qwb = cl.queen("queen_white_b", [7, 1], [7, 1], "Inactive", "white", 410)
    qwc = cl.queen("queen_white_c", [7, 2], [7, 2], "Inactive", "white", 411)
    qwd = cl.queen("queen_white_d", [7, 3], [7, 3], "Inactive", "white", 412)
    qwe = cl.queen("queen_white_e", [7, 4], [7, 4], "Inactive", "white", 413)
    qwf = cl.queen("queen_white_f", [7, 5], [7, 5], "Inactive", "white", 414)
    qwg = cl.queen("queen_white_g", [7, 6], [7, 6], "Inactive", "white", 415)
    qwh = cl.queen("queen_white_h", [7, 7], [7, 7], "Inactive", "white", 416)

    qba = cl.queen("queen_black_a", [0, 0], [0, 0], "Inactive", "black", 717)
    qbb = cl.queen("queen_black_b", [0, 1], [0, 1], "Inactive", "black", 718)
    qbc = cl.queen("queen_black_c", [0, 2], [0, 2], "Inactive", "black", 719)
    qbd = cl.queen("queen_black_d", [0, 3], [0, 3], "Inactive", "black", 720)
    qbe = cl.queen("queen_black_e", [0, 4], [0, 4], "Inactive", "black", 721)
    qbf = cl.queen("queen_black_f", [0, 5], [0, 5], "Inactive", "black", 722)
    qbg = cl.queen("queen_black_g", [0, 6], [0, 6], "Inactive", "black", 723)
    qbh = cl.queen("queen_black_h", [0, 7], [0, 7], "Inactive", "black", 724)
